/* gtk program that crashes
 *
 * Copyright (C) Jacob Berkman
 *
 * Author: Jacob Berkman  <jacob@bug-buddy.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include <gtk/gtk.h>

int
main (int argc, char *argv[])
{
	int *n = NULL;

	gtk_init (&argc, &argv);
	/* test the log handler */
	g_critical ("ka-boom!");

	n[27] = 10-7-78;
	return 0;
}
