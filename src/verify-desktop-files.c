/* gnome program that crashes
 *
 * Copyright (C) Jacob Berkman
 *
 * Author: Jacob Berkman  <jacob@bug-buddy.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <glib.h>
#include <gtk/gtk.h>
#include <bonobo-activation/bonobo-activation.h>

#include "bugzilla.h"

static void
check_bugzilla_info(gpointer key, gpointer value, gpointer user_data)
{
	BugzillaApplication *app = (BugzillaApplication*) value;

	if (!app->bugzilla) {
		g_print ("%s has no Bugzilla info\n", (char*) key);
		return;
	}

	if (!app->product || !app->component) {
		g_print ("%s has not Product/component info\n", (char*) key);
		return;
	}
}



int
main (int argc, char *argv[])
{
	GHashTable *apps;

	gtk_init (&argc, &argv);

	if (!bonobo_activation_is_initialized()) {
		bonobo_activation_init (argc, argv);
	}

	apps = load_applications ();
	
	if (apps == NULL) {
		g_print ("No installed applications!\n");
		return 0;
	}

	g_hash_table_foreach (apps, check_bugzilla_info, NULL);

	return 0;
}
