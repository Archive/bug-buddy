/* 
 * Author:  Brent Smith  <gnome@nextreality.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GDB_BUDDY_H__
#define __GDB_BUDDY_H__

#include <glib.h>

G_BEGIN_DECLS

extern GQuark gdb_buddy_error_quark (void);
#define GDB_BUDDY_ERROR gdb_buddy_error_quark ()

typedef enum {
	GDB_BUDDY_BINARY_NOT_FOUND,
	GDB_BUDDY_GDB_NOT_FOUND,
	GDB_BUDDY_GDB_CMD_NOT_FOUND,
	GDB_BUDDY_GDB_ERROR
} GdbBuddyError;

typedef void (*GdbCallback)(const gchar *stacktrace, gpointer user_data);

guint gdb_get_trace (const char *app, int pid, gpointer user_data, GdbCallback gdb_finish, GError **err);

G_END_DECLS

#endif /* __GDB_BUDDY_H__ */
