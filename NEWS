=======
2.31.92
=======

Bugs fixed:
 - Don't call the call handler if it's not glib's default

Translations:
 - Updated ar Translation (Khaled Hosny)
 - Updated bg Translation (Alexander Shopov)
 - Updated ca Translation (David Planella)
 - Updated da Translation (Kenneth Nielsen)
 - Updated de Translation (Mario Blättermann)
 - Updated en_GB Translation (Philip Withnall)
 - Updated es Translation (Jorge González)
 - Updated et Translation (Ivar Smolin)
 - Updated fr Translation (Claude Paroz)
 - Updated gl Translation (Fran Diéguez, Francisco Diéguez)
 - Updated he Translation (Yaron Shahrabani)
 - Updated hu Translation (Gabor Kelemen)
 - Updated id Translation (Dirgita)
 - Updated it Translation (Milo Casagrande)
 - Updated nb Translation (Kjartan Maraas)
 - Updated pa Translation (A S Alam)
 - Updated pl Translation (Piotr Drąg)
 - Updated pt Translation (Duarte Loreto)
 - Updated ru Translation (Leonid Kanter)
 - Updated sl Translation (Matej Urbančič)
 - Updated sr@latin Translation (Милош Поповић)
 - Updated sr Translation (Милош Поповић)
 - Updated sv Translation (Daniel Nylander)
 - Updated ta Translation (drtv)
 - Updated zh_CN Translation (Aron Xu)
 - Updated zh_HK Translation (Chao-Hsiung Liao)
 - Updated zh_TW Translation (Chao-Hsiung Liao)

======
2.31.3
======
Remove google breakpad code.

2.30.0
Bugs fixed:
 - Fixed #596678, String fixes

Translations:
 - Updated ar Translation (Khaled Hosny)
 - Updated ast Translation (Xandru Armesto Fernandez)
 - Updated bg Translation (Alexander Shopov)
 - Updated bn Translation (Jamil Ahmed)
 - Updated ca Translation (Gil Forcada)
 - Updated cs Translation (Petr Kovar)
 - Updated da Translation (Kenneth Nielsen)
 - Updated de Translation (Christian.Kirbach)
 - Updated el Translation (Kostas Papadimas)
 - Updated en_GB Translation (Bruce Cowan, Philip Withnall)
 - Updated en@shaw Translation (Thomas Thurman)
 - Updated es Translation (Jorge González)
 - Updated et Translation (Priit Laes)
 - Updated eu Translation (Inaki Larranaga Murgoitio)
 - Updated fi Translation (Tommi Vainikainen)
 - Updated fr Translation (Claude Paroz)
 - Updated gl Translation (Fran Diéguez, Antón Méixome)
 - Updated he Translation (Yaron Sharabani)
 - Updated hu Translation (Gabor Kelemen)
 - Updated it Translation (Luca Ferretti)
 - Updated ja Translation (Hideki Yamane (Debian-JP))
 - Updated ko Translation (Changwoo Ryu)
 - Updated lt Translation (Gintautas Miliauskas)
 - Updated lv Translation (Peteris Krisjanis)
 - Updated ml Translation (Praveen Arimbrathodiyil)
 - Updated ms Translation (Ahmed Noor Kader Mustajir Md Eusoff)
 - Updated nb Translation (Kjartan Maraas)
 - Updated nl Translation (Wouter Bolsterlee)
 - Updated nn Translation (Torstein Adolf Winterseth, Åsmund Skjæveland)
 - Updated pa Translation (A S Alam)
 - Updated pl Translation (Piotr Drąg)
 - Updated pt_BR Translation (Flamarion Jorge)
 - Updated pt Translation (Duarte Loreto)
 - Updated ro Translation (Lucian Adrian Grijincu)
 - Updated ru Translation (Leonid Kanter)
 - Updated sk Translation (Roman Mátyus)
 - Updated sl Translation (Matej Urbančič)
 - Updated sr@latin Translation (Miloš Popović)
 - Updated sr Translation (Miloš Popović)
 - Updated sv Translation (Daniel Nylander)
 - Updated ta Translation (drtvasudevan)
 - Updated th Translation (Theppitak Karoonboonyanan)
 - Updated uk Translation (Maxim V. Dziumanenko)
 - Updated vi Translation (Clytie)
 - Updated zh_CN Translation (Tao Wei)
 - Updated zh_HK Translation (Chao-Hsiung Liao)
 - Updated zh_TW Translation (Chao-Hsiung Liao)

2.28.0
  * Updated translations

2.27.92 ("The Fixer")
  * Drop gnome-menus dependency (Vincent Untz)

2.27.1
  * Fix build with GCC 4.40
  * Generate ChangeLog from git-log

2.26.0
  * Don't hardcode a scrollkeeper check in the configure script.

2.25.91
  * Build correctly with --disable-eds.

2.25.2
  * Fix a segfault (Josseline Mouette).
  * Don't free uninitialized memory.

2.25.1 ("The Feel-Good vibe")
  * Drop libgnome and libgnomeui dependencies.
  * Make google-breakpad support optional (but enabled by default).
    Thanks to Sjoerd Simons.
  * Obtain the real path of the crashed process by looking in /proc.
    Thanks to Sam Morris and Matt Keenan.
  * Add an option to delete the included file after bug-buddy has
    processed it.
  * Implement a logger for pasting critical and fatal warnings in the
    stacktraces.
  * Include the loaded GTK+ modules in the stacktraces sent to bugzilla.
  * Update google-breakpad to SVN r290.
  * Compile with all the GLib/GTK+ deprecation flags.

2.24.1
  * Hide the "Send" button after clicking it
  * Make the breakpad module resident (Matthias Clasen)

2.24.0
  * Disable Solaris from the google-breakpad supported OS
  * Adapt gnome-breakpad for loading by XSettings (requires GTK+ 2.14.2
    and gnome-settings-daemon 2.24.0)
  * Many translation updates

2.23.91.1
  * Fix a runtime warning

2.23.91
  * Standardize the desktop file (Pacho Ramos)
  * Clean up X11 includes
  * Many translation updates

2.23.90 ("Live in Paris")

  * Remove dependency on gnome-desktop
  * Fix some typos
  * Many translation updates

2.23.6

  * Remove unused command line switches
  * Update and ship a man page
  * Remove dependency on libgnomecanvas
  * New Tango icon (thanks to Andreas Nilsson, Michael Monreal
    and Sebastian Kraft).
  * Add a context menu to copy the bug URL when a bug has been reported.
  * Add a dialog to warn the user to review the crash description if it is
    too short (thanks to Diego Escalante Urrelo).
  * Many cleanups of the source tree.
  * Some other misc bug fixes
  * Translation updates

2.23.5.1

  * Correctly install bug-buddy.png

2.23.5

  * Port to GIO (Cosimo Cecchi)
  * Completely remove the NetworkManager dependency (Cosimo Cecchi)
  * Fix some leaks (Hans Petter Jansson)
  * Many misc bug fixes
  * Translation updates

2.19.91 ("Sampaio")

  * Fixes to the new minidump code.
  * Some more bug fixes :)

2.19.0 ("Young Explorer")

  * Build a GTK_MODULE invoking bug-buddy on segfaults.
  * Add support for dumping a minidump file if no debug symbols are present.

2.18.0 ("Atajo")

  * Removed Application from .desktop file (Christian Kirbach)

2.17.4 ("Oslo by night")

  * Don't register with the session manager (Dan Winship)
  * Fix invalid read in the gmenu code (Mark McLoughlin)
  * Search for desktop files in the autostart dir too (Fer)
  * Compiler warning fixes (Christian Persch)
  * String fixes (Priit Laes, Andre Klapper) 
  * Updated translations:
    - Updated ar: Djihed Afifi
    - Updated be: Ihar Hrachyshka
    - Updated bg: Vladimir Petkov
    - Updated da: Peder Bach
    - Updated dz: Pema Geyleg
    - Updated en_CA: Adam Weinberger
    - Updated en_GB: David Lodge
    - Updated et: Priit Laes
    - Updated fi: Illka Tuohela
    - Updated fr: Robert-André Mauchin and Jonathan Ernst
    - Updated he: Yair Hershkovitz
    - Updated ko: Changwoo Ryu
    - Updated mk: Jovan Naumovski
    - Updated nb: Kjartan Maraas
    - Updated pl: The GNOME PL Team
    - Updated pt: Duarte Loreto
    - Updated pt_BR: Og Maciel
    - Updated sl: Matic Zgur
    - Updated sv: Daniel Nylander
    - Updated th: Theppitak Karoonboonyanan
    - Updated uk: Maxim Dziumanenko
    - Updated vi: Clytie Siddall
    - Updated zh_HK: Chao-Hsiung Liao
    - Updated zh_TW: Chao-Hsiung Liao

2.17.3 ("Paris connection")

  * Removed status bar
  * Lock some text on the report
  * Detect sensitive words (regarding privacy) on the report
    and add a warning and highlight them
  * Lot of UI and strings clean ups (Dennis Cranston and Andre
    Klapper)
  * Fix leaks
  * Don't submit reports with less than 3 steps in the trace
  * Do not block the UI while gdb is working!
  * Reworked the email check code (Alex Jones)
  * Updated Translations:
    - Updated nb: Kjartan Maraas
    - Updated sv: Daniel Nylander
    - Updated hu: Gabor Kelemen
    - Updated th: Theppitak Karoonboonyanan
    - Updated fi: Ilkka Tuohela
    - Updated es: Francisco Javier F. Serrador
    - Updated he: Yair Hershkovitz

2.17.2 ("3rd coin")

  * Fix last minute problems on 2.17.1 with the link button
  * Don't set priority/severity on non-crashers.

2.17.1 ("one more coin")

  * Fix unreleased 2.17.0 version: Include po files on the
    tarball.

2.17.0 ("Coin, operated boy")

  * Add more system info to bug-buddy reports (Alexander Larsson)
  * Misc fixes (Kjartan Maraas, Ali Sabil, Brian Pepple, 
    Stephane LOEUILLET, Rob Bradford, Clytie Siddall,
    Luca Cavalli and myself)
  * Ask the user to fill the report in English (Claudio Saavedra and
    Mariano Suárez-Alvarez)
  * Add a X-GNOME-Bugzilla-ExtraInfoScript/bugzilla:extra_info_script
    field to allow applications to run a custom script to collect
    more information (based on an idea from Federico Mena-Quintero)
  * Only load current application icon. Improves performace from 10%
    to 90%.
  * Limit stack traces deep (Ruben Vermeersch)
  * Add a help button (Brent Smith)
  * Use a new window to allow the user review the information. Allow him
    to copy/edit this information.

2.16.0 ("bye bye dyckola")

  * Fix a crash when focusing details-view

2.15.92 ("Lumbago")

  * drop local xml-rpc functions
  * Port to GKeyFile
  * Bug fixes
  * Show window while collecting .desktop files
  * Use some heuristics as report summary

2.15.90 ("IKEA")

  * Support for --include option
  * Support for sending previously non-sent bug reports.
  * Use own .menu file for reading all applications
  * when invoked with --package and without --pid, don't assume it
    is a crash.
  * use theme-friendly icons
  * Fixes

2.15.0 ("sendmail, no mas")

  * bug-buddy-xmlrpc branch merged to HEAD

2.13.0 ("Mañana, mañana!")

  * Fixes
	- Update treeview headers when switching between
	products/applications (Matthias Clasen)
	- Fix LSB distro detection (Fernando)
	- Set GnomeFileEntry in save mode when saving the report
	(Fernando)
	- HIG fixes (Christian Perch)
	- Center the main window (Christian Neumair)
  * New and updated translations:
	- Alexander Shopov (bg)
	- Hendrik Richter (de)
	- Adam Weinberger (en_CA)
	- Francisco Javier F. Serrador (es)
	- Ivar Smolin (et)
	- Priit Laes (et)
	- Ilkka Tuohela (fi)
	- Christophe Merlet (fr)
	- Ignacio Casal Quinteiro (gl)
	- Ankit Patel (gu)
	- Gabor Kelemen (hu)
	- Alessio Frusciante (it)
	- Kjartan Maraas (nb)
	- Tino Meinen (nl)
	- Slobodan D. Sredojevic (sr, sr@Latn)
	- Theppitak Karoonboonyanan (th)
	- Clytie Siddall (vi)
	- Woodman Tuen (zh_HK)
	- Abel Cheung (zh_TW)

2.12.2 ("Il vulcano")
  * New and updated translations:
	- Ales Nyakhaychyk (be)
	- Vladimir Petkov (bg)
	- Khandakar Muajhidul Islam (bn)
	- Miloslav Trmac (cs)
	- Adam Weinberger (en_CA)
	- Francisco Javier F. Serrador (es)
	- Priit Laes (et)
	- Iñaki Larrañaga (eu)
	- Ilkka Tuohela (fi)
	- Ignacio Casal Quinteiro (gl)
	- Ankit Patel (gu)
	- Gabor Kelemen (hu)
	- Takeshi AIHANA (ja)
	- Timur Zhamakeev (ky)
	- Kjartan Maraas (nb)
	- Tino Meinen (nl)
	- Åsmund Skjæveland (nn)
	- Kjartan Maraas (no)
	- Guilherme de S. Pastore (pt_BR)
	- Dan Damian (ro)
	- Marcel Telka (sk)
	- Laurent Dhima (sq)
	- Christian Rose (sv)
	- Theppitak Karoonboonyanan (th)
	- Maxim Dziumanenko (uk)
	- Wang Jian (zh_CN)

2.12.1 ("iParty 7")
 * Fixes:
	- Remove duplicate entries from app list (Matthias Clasen)

2.12.0 ("My fatal fate")
  * New and updated translations:
	- Jordi Mallach (ca)
	- Rhys Jones (cy)
	- Yair Hershkovitz (he)
	- Gabor Kelemen (hu)
	- Žygimantas Beručka (lt)
	- Michiel Sikkes (nl)
	- Leonid Kanter (ru)

2.11.92 ("")
  * New and updated translations:
	- Miloslav Trmac (cs)
	- Kostas Papadimas (el)
	- Adam Weinberger (en_CA)
	- Francisco Javier F. Serrador (es)
	- Priit Laes (et)
	- Ilkka Tuohela (fi)
	- Ankit Patel (gu)
	- Yair Hershkovitz (he)
	- Gabor Kelemen (hu)
	- Takeshi AIHANA (ja)
	- Kjartan Maraas (nb)
	- Kapil Timilsina (ne)
	- Kjartan Maraas (no)
	- Gnome PL Team (pl)
	- Afonso Celso Medina (pt_BR)
	- Duarte Loreto (pt)
	- Dan Damian (ro)
	- Marcel Telka (sk)
	- Laurent Dhima (sq)
	- Theppitak Karoonboonyanan (th)
	- Baris Cicek (tr)
	- Maxim Dziumanenko (uk)
	- Clytie Siddall (vi)
	- Wang Jian (zh_CN)
	- Woodman Tuen (zh_TW)
  * New ukrainian translation (Maxim Dziumanenko)

2.11.1 ("Que fort")
  * Fixes;
	- Fix gcc4 compiler warnings (Kjartan Maraas)

  * Impovements;
	- Ported to GtkAboutDialog (Kjartan Maraas)
	- Port to GtkComboBox (Michael Gossard)
	- Sort frequently reported bugs list (Michael Gossard)
	- Add support for LSB distributions

2.11.0
  * Update to the new menu api (Mark McLaughlin)
  * Fix a crash on startup (Olav Vitters)
  * Translation updates

2.10.0 ("La movida")
  * Translations:
	- Some translations updates

2.9.92 ("Magical Fenix")
  * Translations:
	- Lot of translations updates
	- New Ukrainian translation of the documentation by 
	  Maxim V. Dziumanenko <mvd@mylinux.com.ua>

2.9.91 ("Hänsel und Gretel")
  * Fixes:
	- Port to the new libmenu API to get the list of available applications.
	- Fix the applets loading.

2.9.3 ("Wild Rose")
  * Translations:
	- Updated Italian, Chinese, Swedish, Estonian and German
	  translations.

2.9.2 ("Tres Jotas")
  * Fixes:
	- Use the command line option --include-file to include a file
	- Skip the product/component pages if the application is
          passed on the command line (also show its name in the 
          intro page)

  * Improvements:
	- Added a kill command line argument for killing that pid after
	  the bug report is sent (this is useful for calling bud-buddy
	  inside your application when a non-segfaulf crash occurs, like
	  in python code or scheme, etc...)

2.8.0 ("Chicote")

  * Fixes:
	- Disable build of outdated documentation translations
	- Fix core file MIME handling
	- Open documentation in the right section (Shaun McCance)

  * Improvements:
	- Update documentation (Shaun McCance)

  * Translations:
	- More final updates

2.7.92 ("Ojos verdes)

  * Translations:
	- Some final updates

2.7.91 ("budyboop")

  * Translations:

	- Updated Albanian translation (Laurent Dhima)
	- Updated Swedish translation (Christian Rose)
	- Updated az translation (MÉtin Æmirov)
	- Added Bosnian translation (Kenan HadÅ¾iavdiÄ)
	- Updated Malay translation (Hasbullah Bin Pit)
	- Updated Russian translation (Leonid Kanter)
	- Updated Simplified Chinese translation (Funda Wang)
	- Updated Ukrainian translation (Maxim Dziumanenko)
	- Updated Catalan translation (Jordi Mallach)
	- Updated Finnish translation (Sami Pesonen)
	- Updated Basque translation (Iñaki Larrañaga)
	- Updated Korean translation (Changwoo Ryu)
	- Updated Albanian translation (Laurent Dhima)
	- Updated Portuguese translation (Duarte Loreto)
	- Updated Hindi translation (Guntupalli Karunakar)
	- Updated Brazilian Portuguese translation (Estêvão Samuel Procópio)
	- Updated Danish translation (Ole Laursen)
	- Updated Bulgarian translation (Rostislav Raykov)
	- Updated German translation (Christian Neumair)
	- Updated Serbian translation (Danilo Å egan)
	- Updated Hungarian translation (Andras Timar)
	- Updated Tamil Translation (Dinesh Nadarajah)
	- Updated Japanese translation (Takeshi AIHANA)

2.7.0 ("Lully")
  * Fixes:

	- Fix some strings after the freeze (#136191, #140053)
	- Add scroll bar to description bug text box.

  * Improvements:

	- Code clean up by Simon Frankau:
		* Use buddy_error for error dialogs
		* Removed unused variables
		* Fix memmory leaks in gdb code
		* state machine rearranged

2.6.0 ("Almodóvar did it again!")

  * Fixes:

	- Fix a compiler error (bug #135625)

  * Translations:

	- Updated Brasilian translation (Gustavo Noronha Silva)
        - Updated Bengali translation (Khandakar Mujahidul Islam)
        - Added Punjabi translation (Jaswinder Singh Phulewala)
        - Updated Welsh translation (Telsa Gwynne)
        - Added British translation (Gareth Owen)
	
2.5.92 ("We are the champions")

  * Fixes:

	- Update to application/x-core MIME type

	- Disable Back button when debugging is the first page

  * Improvements:

	- Update translated docs (Glynn Foster)

	- 
2.5.90 ("I love it when a plan comes together")

  * Fixes;

	- Fix a crash when going back from the email page. Thanks to Simon
	  Frankau for fixing this (bug #126449)

	- Fix a crash when checking versions twice (bug #134167)

	- Load product list after failing guessing.

  * Improvements:

	- Kill the confirmation dialog

	- Set the FileChooser in save mode for saving bug reports

2.5.3 ("No name release")

  * Fixes:

	- Fix Application cooment (Vincent Untz)

	- Some build fixes (Jason Leach, Alexander Winston, Vincent Berger)

	- Fix typos in schemas

  * Improvements:

	- If GNOME version is older than 6 months suggest upgrading

	- Use GtkFileChooser in entries

2.5.2 ("Franco Corelli")

  * Fixes:

	- Preserve bug description

	- Don't hardcode FileChooser size

	- Don't add closed products

  * Improvements:

	- Report Irix, Fedora and Gentoo "distributions"

	- Show only Applications we can submit bugs to bugzilla (Bug #129138)

	- Use a radio button to switch between Applications/Porducts view

	- Add application/product name to component page (Bug #128349)

	- Ported to gtk_icon_theme_*

	- Add translators to about dialog

	- Port configuration to GConf

2.5.1 ("Mi mamá prefiere GNOME")

  * Fixes:

	- Load again component list

  * Improvements:

	- Use GtkFileChooser in the non-glade stuff (Jan Arne Petersen)

2.5.0 ("He tardado 10 minutos en atarme las botas")

  * Fixes:

	- Fix some mem leaks (Kjartan Maraas, bug #125396)

	- Disable deprecations (Sivaiah N)

	- Fix crash when trying to debug a non-found program (Vincent Untz, 
	  bug #115147)
	
  * Improvements:

	- Don't show up to three windows at a time (Vincent Untz, bug #110817)


2.4.0 ("Galactic")

  * Translations:

	Sami Pesonen (Finnish)
	Dafydd Harries (Welsh)

2.3.5 ("La Espiral")

  * Translations:

	Duarte Loreto (Portuguese)
	Sanlig Badral  (Mongolian)
	Stanislav Visnovsky  (Slovak)
	Kjartan Maraas  (Norwegian)
	Metin Amiroff (Azerbaijani)
	Evandro Fernandes Giovanini (Brazilian Portuguese)
	Priit Laes (Estonian)
	Guntupalli Karunakar  (Hindi)
	Sami Pesonen  (Finnish)
	Funda Wang (Chinese)
	Alessio Frusciante (Italian)
        Francisco Javier F. Serrador (Spanish)
	Andras Timar  (Hungarian)


2.3.4 ("Pinochio")

  * Fixes:

	- Workaround for a bugzilla.gnome.org bug
 
	- Fix a free (bug #117806)

	- Fix a crash (bug #117809)

	- Unify copyright strings (Christian Neumair)

  * Improvements:

	- Fill version entry with unspecified if version is not know 
	after selecting a product (bug #117934)
 

2.3.3 ("I have given up smoking")

  * Fixes:
	
	- Strings fixes and corrections (Thanks to Luis Villa)

	- Fix for #116085 (Using Gnome Distributor instead or Gnome-Distributor
	  made fail the report). Thanks to Andrew Sobala for tracking this.
	
	- Fix a crash when reporting non-bugzilla bugs without Description field

	- When handling gdb input, use ISO-8859-1 as orig encoding (gdb default) and 
	  use a fallback for converting them into UTF-8 (Fixes problems with some
	  stacks with non-translatable characters)

	- Without network connection, use latest donwloaded bugzilla files is they are
	  newer then the system installed ones.

	- Correctly set "enhacement" and "critial" severities for feature requests and 
	  crashes respectively.

	- Set package version in the "crash+we know product+we don't know component" 
	  scenario

	- Use again applications-all-users:/// now that gnome-vfs bug is fixed

  * Improvements:

	- Add accelerators to every option

	- Final HIGfy work 
	
	- Remove confirmation dialog before submiting a bug

	- Use different text templates for any kind of bug

	- Use a combo menu for version entry (filled with bugzilla valid versions for that product)


2.3.2 ("In the neighbourhood")

  * Fixes:
	
	- Fix crashin in products view (bug #114712), thanks to Ximian folks
	  for this.

	- Fix for #104357, thanks to Jens Askengren 

	- Two typos fixed by Abel Cheung

  * Improvements:

	- Read X-GNOME-Bugzilla-version from .desktop files and set version
	  for the bug report.
	
	- Don't add dupplicated applications (ie: If we have the same
	  application with differents .desktop files)

	- Don't allow the user to modify Bugzilla headers in the email page

2.3.1 ("My brown paper bag")

  * Fixes
:
	- Go to debug page when invoked from crash.

2.3.0 ("Sex, lies and Div-Xs")

  * Fixes:

	- Print the progress label only when all files/sizes are calculated
	  (Fixes #108408)

	- Use "Distributor" from gnome-version.xml instead of "Vendor"


  * Improvements:
	
	- Use the bug-buddy icon in the about dialog (Fixes #104445, Alex
	  Duggan)

	- By popular request, include distribution version in the bug report.

	- New first page when invoked from scratch, asking the user to select
	  the "kind" of bug he wants to report. Set priorities and keywords
	  based on this info.


2.2.102 ("Oh na na na"):

  * Fixes: 

	- Fix a crashing bug when sending a bug report for a product without
	  X-Gnome-Bugzilla stuff in its .desktop file (#104599)

2.2.101 ("Fidelio"):

  * Fixes:

	- Get gdb backtraces as non-UTF8 so we can read invalid UTF-8 strings
          (fixes #102493).


2.2.100 ("I should be there, baby!"):

  * Fixes:

	- Workaround for all-applications gnome-vfs bug.

	- Change "Refresh" button to "Update" (bug #101693).

	- Some text corrections (bugs #86410, #99564)


  * Improvements:

	- When reporting a bug from scratch, after the app is selected, if 
	  the component is known, preselect it. Thanks to Kristian Rietveld

	- Use the GNOME 2 cool logo for GNOME products.

2.2.99 ("Rain Dogs"): 

  * Fixes:

	- fix incorrect url (bug #86711)

	- fix a leak (bug #95981, Kjartan)

	- make file saving  (bug #84914)

	
  * Improvements:

	- Try to update bugzilla files only once a day, and get first md5sums
	  to check if it's really needed. New update dialog (HIG compliant) at
	  startup time.

	- When invoked from crash, skip Application/product and component
	  step, because we have the info from .dekstop file. If invoked from
	  scratch, skip only product step. We should be able to autoselect the
	  component, but cannot do with gtktreeview.

	- Get GNOME version from $prefix/share/gnome-about/gnome-version.xml
	
	- Use hash tables for components and products

	- Present the user a list of applications from all-applications:///
	  Get bugzilla component and product from the .desktop entries (and
	  query bonobo-activation for applets).
	  Allow to choose between Applications of Bugzilla products.

	- Port to GnomeIconTheme

2.2.1:

  * Fixes:

	- make stack saving work (bug #80334)

	- fix 'cancelling bug-buddy makes old app hang' (bug #86404)

	- gdb truncation bug (#82722)

  * Improvements:

	- prompt the user before overwriting a file (#84914)

2.2.0:

  * Fixes:

	- mime handling for core files should work now (Alex Graveley)

  * Improvements:

	- Docs updated for GNOME 2 version (Eric Baudais, Chris
	Lyttle)

	- Man page from Debian (David LaBissoniere)

2.1.6:

  * Fixes:

	- Fixed to not totally break with automake 1.[56] (bug #74390)

	- Don't allow selections of the mostfreq list (Wayne Schuller,
	bug #77277)

	- Don't go to the mostfreq page when it is empty (bug #76405)

	- Comply with UI guidelines for some dialogs (Archit Baweja,
	bug #74816)

  * Improvements:

	- See if removing AM_PROG_LIBTOOL breaks anything

	- Use the new network properties thing instead of our own
	dialog

2.1.5:

  * Fixes:

	- fix .desktop file for gnome 2 install (Seth Nickell)

	- fix dynamic export ldflags (bugs #77035 and #76384, thanks
	to Dan Winship)

	- install .omf files

	- handle g_convert() not returning anything

	- fix icon in druid

  * Improvements:

	- uses glade-2 now, so libglade-convert is no longer needed

	- add ability to save/copy stack trace w/o having to go
	through all the steps (fixes #77891)

	- run gdb with 'bt full' rather than just 'bt' (fixes #78675,
	havoc)

2.1.4:

  * Fixes:

	- Back button isn't the default button

	- Fix possible crash when saving to a file, and display which
	file it was saved to at the end (Wayne Schuller, bug #73675)

	- Update docs for GNOME 2.0 version (Kevin Conder, bug #71189

  * Improvements:

	- Support for GNOME's most frequently reported bugs

	- Print which app generated the core file with the bug report

2.1.3:

  * Fixes:

	- don't allow mails from root@ (b.g.o #57490)

  * Improvements:

	- redesigned druid to be easier to use and more sane

	- respects themes a bit more and looks more like GnomeDruid

2.1.2:

  * Fixes:

	- convert xml to UTF-8 so that non-ascii characters display
	  correctly

  * Improvements:

	- display frequently reported bugs

	- build with less deprecated functiosn

	- use new GTK 2.0 button layout

	- add a tag to the bugzilla email if a gnome 2 app crashed

2.1.1:

  * Fixes:

	- Clean up GtkDialogs

	- Other misc. porting fixes and cleanups.

2.1.0:

  * Improvements:

	- port to GNOME 2.0

2.0.8:

  * Fixes:

	- don't pass --nw anymore: seems to break things

2.0.7:

  * Fixes:

	- pass --nw to gdb (from Leonard Norrgard  <vinsci@bugg.lnorrgard.com>)

	- wrap mail bodies at 72 chars again (me)

	- have gdb get stack traces from all threads (zucchi mentioned
	  the command on irc, but i don't know where he found it)

2.0.6:

  * Fixes:

	- translation updates

  * Improvements:

	- allows bugzillas to use different parts of the config.xml
	  for "severity"

2.0.5:

  * Fixes:

	- touch files after we download them so that the cache
	  actually works.

2.0.4:

  * Fixes:

	- set the From: header

	- make sure the user knows the bug is going to be submitted
	  before it is sent

2.0.3:

  * Fixes:

	- libxml include path fixes

2.0.2:

  * Fixes:

	- Ximian Bugzilla support

	- other small bugs

2.0.1:

  * Fixes:

	- Fix a bug where the cache never gets updated.

	- Don't access widgets after they have been destroyed when
	  saving config.

2.0:

  * Rewritten to support bugzilla

  * The UI should be nicer too

1.2:

 * Fixes:

	- bug #29310

 * Improvements:

	- Added a --include argument to set which file is included in
	  the report

1.1:

 * Fixes:

	- lots of compilation warnings

 * Improvements:

	- more strict email checks

	- IRIX support

	- doesn't call gdb on directories (for evolution)

1.0:

 * Fixes:

	- bug #11485

	- some other bugs

	- make the text widget in the miggie combo a bit smarter

	- fixed some i18n issues
0.90:

 * Fixes:

	- bugs #10159 and #9493

 * Improvements:

	- window icon is now set

0.9:

 * Fixes:

	- GTK warnings fixed

 * Improvements:

	- gnome-mime is now used to try to guard against non-text
	  files being sent to the bug tracker

0.8:

* Fixes:

	- all bugs on bugs.gnome.org that can be fixed are either
	  fixed or somewhat addressed

	- Checks for version on TuboLinux

* Improvements:

	- much of the .glade was redone

	- it is now possible to cc yourself on bug submitions

	- you get a preview of the message before you send it

	- (not finished) user documentation

	- you can submit bugs to arbitrary people now

	- Helix Code BTS

0.7:

* Fixes:

	- fixed bug #6013

	- minor aesthetic tweaks

	- translations

0.6:

* Fixes: 

	- I don't think there needed to be any :)

* Improvements:

	- Support for Debian and KDE's bug tracking system

	- UI tweaks

	- including text files in the report is now possible

0.5:

* Fixes:

	- text areas now have their text wrapped when you send

	- correctly detect debian systems

* Improvements:

	- it's now possible to add information to an existing bug report

	- can now query dpkg on debian systems

	- now detects Linux-Mandrake  (patches readily accepted for 
	detecting other distributions)

	- less druid pages, and prettier UI

	- availability of .deb and .rpm packages

0.4:

* Fixes:

	- possible crash when clicking on a clist with a blank 2nd
	column

	- spelling errors

* Improvements:

	- reorganized the druid a little

	- code reorg

	- can now stop/refresh the gdb process

0.3 release:

No release announcement this time, I guess.

* Fixes:

	- fix duplicate message sendome with qmail

	- make sure the user wants to overwrite a file

	- more descriptive error message when the .glade
	file doesn't exist

	- do some stuff for when we aren't installed in 
	the gnome prefix

	- fix some warnings etc. for intl.

	- spec.in file

0.2 release:

Hello,

I've made a couple of minor (but kind of important) fixes to bug-buddy.

* Availability:

	- ftp://ftp.gnome.org/pub/GNOME/sources/bug-buddy/

* Changes:

	- should more correctly determine the gnome-core version

	- change 'severe' severity to 'grave'

	- better mime regex

Enjoy,
Jacob

0.1 release:

Hello bug hunters,

I've just released the first version of bug-buddy, a graphical
bug reporting utility.

The goal of bug-buddy is to make reporting bugs very simple and easy
for the user, while making the reports themselves more useful and
informative for the developer.

* Availability:

	- ftp://ftp.gnome.org/pub/GNOME/sources/bug-buddy/

* Features:

	- automatic stack trace gathering, either from a core file
	  or from a crashed application.  No messing with gdb :)

	- determines the versions of different components installed
	  on your machine

	- can be started from gmc (double clicking on a core file)
	  and from the crash dialog, if you have gnome-libs 1.0.53
	  (October GNOME) or higher installed

* Requirements:

	- libglade: 0.5 or later required (CVS version has some layout
	  fixes)

	- gdb is required to obtain a stack trace automatically

	- gnome-libs 1.0.53 or higher recommended

* Notes:

	- yes, some information about your system will be submitted
	  to the bug tracking system.  This is limited to versions of
	  system and gnome components, but I figure I should mention it.

	- bug reports for bug-buddy are appreciated

Enjoy,
Jacob
