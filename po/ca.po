# bug-buddy translation to Catalan.
# Copyright © 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007 Free Software Foundation, Inc.
# Softcatalà <info@softcatala.org>, 2000, 2001.
# Jordi Mallach <jordi@sindominio.net>, 2002, 2003, 2004, 2005, 2006.
# Xavier Conde Rueda <xavi.conde@gmail.com>, 2006, 2007.
# Gil Forcada <gil.forcada@guifi.net>, 2008, 2009, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: bug-buddy 2.13.92\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-08-22 09:33+0200\n"
"PO-Revision-Date: 2010-08-22 09:34+0200\n"
"Last-Translator: Gil Forcada <gilforcada@guifi.net>\n"
"Language-Team: Catalan <tradgnome@softcatala.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n!=1;\n"

#: ../data/bug-buddy.gtkbuilder.h:1
msgid "<b>What _were you doing when the application crashed?</b>"
msgstr "<b>_Què fèieu quan l'aplicació ha fallat?</b>"

#: ../data/bug-buddy.gtkbuilder.h:2
msgid "<b>Your _email address:</b> "
msgstr "<b>La vostra adreça _electrònica:</b> "

#: ../data/bug-buddy.gtkbuilder.h:3
msgid ""
"<small><i><b>Note:</b>  Sensitive information may be present in the crash "
"details.  Please review the crash details if you are concerned about "
"transmitting passwords or other sensitive information.</i></small>"
msgstr ""
"<small><i><b>Nota:</b> Segurament hi pot haver dades sensibles en els "
"detalls de la fallada. Reviseu i editeu la informació si us preocupa "
"transmetre contrasenyes i altres dades sensibles.</i></small>"

#: ../data/bug-buddy.gtkbuilder.h:4
msgid "<span size=\"xx-large\"><b>Bug reporting tool</b></span>"
msgstr "<span size=\"xx-large\"><b>Eina per informar d'errors</b></span>"

#: ../data/bug-buddy.gtkbuilder.h:5 ../src/bug-buddy.c:1942
msgid "Bug Buddy"
msgstr "Bug Buddy"

#: ../data/bug-buddy.gtkbuilder.h:6
msgid "C_opy"
msgstr "C_opia"

#: ../data/bug-buddy.gtkbuilder.h:7
msgid "Review Crash Details"
msgstr "Revisa els detalls de la fallada"

#: ../data/bug-buddy.gtkbuilder.h:8
msgid "Send _other pending crash reports"
msgstr "Envia també _altres informes d'error pendents"

#: ../data/bug-buddy.gtkbuilder.h:9
msgid "_Review Crash Details"
msgstr "_Revisa els detalls de la fallada"

#: ../data/bug-buddy.gtkbuilder.h:10
msgid "_Send"
msgstr "_Envia"

#: ../data/bug-buddy.desktop.in.in.h:1
msgid "Bug Report Tool"
msgstr "Eina d'informe d'errors"

#: ../data/bug-buddy.desktop.in.in.h:2
msgid "Report a bug in GNOME-based applications"
msgstr "Informeu d'errors en aplicacions del GNOME"

#: ../data/bug-buddy.schemas.in.h:1
msgid "Bug reporter name"
msgstr "Nom de l'informador de l'error"

#: ../data/bug-buddy.schemas.in.h:2
msgid "Email Address"
msgstr "Adreça electrònica"

#: ../data/bug-buddy.schemas.in.h:3
msgid ""
"Email Address for submitting bug reports to GNOME Bugzilla. This address "
"will be used for correspondence regarding the bug you are submitting. If you "
"already have a GNOME Bugzilla account, please use it as your Email Address."
msgstr ""
"L'adreça de correu amb la qual enviar errors al Bugzilla del GNOME. Aquesta "
"adreça s'utilitzarà per a la correspondència relacionada amb l'error que "
"enviareu. Si ja teniu un compte al Bugzilla del GNOME, utilitzeu-la com a la "
"vostra adreça de correu."

#: ../data/bug-buddy.schemas.in.h:4
msgid "File to store unsent bug reports."
msgstr "Fitxer on desar els informes d'error no enviats."

#: ../data/bug-buddy.schemas.in.h:5
msgid ""
"File where bug-buddy will save your bug report in case it can't be sent "
"immediately to Bugzilla."
msgstr ""
"Fitxer on voleu que el Bug Buddy desi els informes d'error en cas que no els "
"envieu immediatament al Bugzilla."

#: ../data/bug-buddy.schemas.in.h:6
msgid "GTK+ module for crash collection support."
msgstr "Mòdul del GTK+ per la gestió de recol·lecció informació de fallades."

#: ../data/bug-buddy.schemas.in.h:7
msgid "Real name of user reporting the bug."
msgstr "Nom real de l'usuari que està enviant l'informe d'error."

#: ../data/bug-buddy.schemas.in.h:8
msgid ""
"This key determines the GTK+ module to load for crash collection support."
msgstr ""
"Aquesta clau indica quin mòdul de GTK+ s'ha de carregar per a la gestió de "
"recol·lecció informació de fallades."

#: ../src/bug-buddy.c:82
msgid "GNOME Bug Buddy"
msgstr "Bug Buddy del GNOME"

#: ../src/bug-buddy.c:90
msgid "Package containing the program"
msgstr "Paquet que conté el programa"

#: ../src/bug-buddy.c:90
msgid "PACKAGE"
msgstr "PAQUET"

#: ../src/bug-buddy.c:91
msgid "File name of crashed program"
msgstr "Nom de fitxer del programa que ha fallat"

#: ../src/bug-buddy.c:91 ../src/bug-buddy.c:93
msgid "FILE"
msgstr "FITXER"

#: ../src/bug-buddy.c:92
msgid "PID of crashed program"
msgstr "PID del programa que ha fallat"

#: ../src/bug-buddy.c:92
msgid "PID"
msgstr "PID"

#: ../src/bug-buddy.c:93
msgid "Text file to include in the report"
msgstr "Fitxer de text que s'ha d'incloure a l'informe"

#: ../src/bug-buddy.c:94
msgid "Delete the included file after reporting"
msgstr "Suprimeix el fitxer inclòs després d'informar"

#: ../src/bug-buddy.c:376
msgid "Copy _Link Address"
msgstr "Copia l'adreça de l'_enllaç"

#: ../src/bug-buddy.c:425
#, c-format
msgid "Bug Buddy was unable to view the link \"%s\"\n"
msgstr "El Bug Buddy no ha pogut visualitzar l'enllaç «%s»\n"

#: ../src/bug-buddy.c:460
msgid ""
"There was a network error while sending the report. Do you want to save this "
"report and send it later?"
msgstr ""
"Hi ha hagut un error de xarxa mentre s'enviava l'informe. Voleu desar-lo i "
"enviar-lo més tard?"

#: ../src/bug-buddy.c:463
msgid ""
"Please ensure that your Internet connection is active and working correctly."
msgstr ""
"Assegureu-vos que la connexió a Internet és activa i funciona correctament."

#: ../src/bug-buddy.c:550 ../src/bug-buddy.c:1184
msgid "_Close"
msgstr "_Tanca"

#: ../src/bug-buddy.c:594
#, c-format
msgid ""
"A bug report detailing your software crash has been sent to GNOME. This "
"information will allow the developers to understand the cause of the crash "
"and prepare a solution for it.\n"
"\n"
"You may be contacted by a GNOME developer if more details are required about "
"the crash.\n"
"\n"
"You can view your bug report and follow its progress with this URL:\n"
msgstr ""
"S'ha enviat un informe d'error del programari al GNOME. Aquesta informació "
"ajudarà els desenvolupadors a entendre perquè ha fallat, i així poder trobar-"
"hi una solució.\n"
"\n"
"És possible que un desenvolupador del GNOME es posi en contacte amb vós si "
"es necessiten més detalls sobre la fallada.\n"
"\n"
"Podeu veure el vostre informe d'error i el seu progrés en aquest URL:\n"

#: ../src/bug-buddy.c:605
msgid ""
"Bug Buddy encountered an error while submitting your report to the Bugzilla "
"server.  Details of the error are included below.\n"
"\n"
msgstr ""
"El Bug Buddy no ha pogut enviar el vostre informe al servidor Bugzilla. Els "
"detalls de l'error s'inclouen a sota.\n"
"\n"

#: ../src/bug-buddy.c:611
#, c-format
msgid ""
"Bugzilla reported an error when trying to process your request, but was "
"unable to parse the response."
msgstr ""
"El Bugzilla ha informat d'un error en intentar processar la vostra petició, "
"però no ha estat capaç d'analitzar la resposta."

#: ../src/bug-buddy.c:614
#, c-format
msgid "The email address you provided is not valid."
msgstr "Heu proporcionat una adreça de correu invàlida."

#: ../src/bug-buddy.c:616
#, c-format
msgid ""
"The account associated with the email address provided has been disabled."
msgstr ""
"S'ha inhabilitat el compte associat amb l'adreça de correu proporcionada."

#: ../src/bug-buddy.c:619
#, c-format
msgid ""
"The product specified doesn't exist or has been renamed.  Please upgrade to "
"the latest version."
msgstr ""
"El producte que heu especificat no existeix o el seu nom ha canviat. Heu "
"d'actualitzar-vos a la darrera versió."

#: ../src/bug-buddy.c:622
#, c-format
msgid ""
"The component specified doesn't exist or has been renamed.  Please upgrade "
"to the latest version."
msgstr ""
"El component que heu especificat no existeix o el seu nom ha canviat. "
"Actualitzeu-vos a la versió més recent."

#: ../src/bug-buddy.c:625
#, c-format
msgid ""
"The summary is required in your bug report. This should not happen with the "
"latest Bug Buddy."
msgstr ""
"Cal un resum a l'informe d'error. Això no hauria de succeir amb el Bug Buddy "
"més recent."

#: ../src/bug-buddy.c:628
#, c-format
msgid ""
"The description is required in your bug report. This should not happen with "
"the latest Bug Buddy."
msgstr ""
"Cal la descripció a l'informe d'error. Això no hauria de succeir amb el Bug "
"Buddy més recent."

#: ../src/bug-buddy.c:631
#, c-format
msgid ""
"The fault code returned by Bugzilla is not recognized. Please report the "
"following information to bugzilla.gnome.org manually:\n"
"\n"
"%s"
msgstr ""
"No s'ha reconegut el codi de fallada retornat pel Bugzilla. Feu arribar la "
"següent informació manualment a bugzilla.gnome.org:\n"
"\n"
"%s"

#: ../src/bug-buddy.c:638
#, c-format
msgid ""
"Server returned bad state.  This is most likely a server issue and should be "
"reported to bugmaster@gnome.org\n"
"\n"
"%s"
msgstr ""
"El servidor ha retornat un valor incorrecte. És molt probable que sigui "
"degut a un problema del servidor, i se n'hauria d'informar a bugmaster@gnome."
"org\n"
"\n"
"%s"

#: ../src/bug-buddy.c:643
#, c-format
msgid ""
"Failed to parse the XML-RPC response.  Response follows:\n"
"\n"
"%s"
msgstr ""
"No s'ha pogut analitzar la resposta XML-RPC. La resposta a continuació:\n"
"\n"
"%s"

#: ../src/bug-buddy.c:647
#, c-format
msgid ""
"An unknown error occurred.  This is most likely a problem with Bug Buddy. "
"Please report this problem manually at bugzilla.gnome.org\n"
"\n"
msgstr ""
"S'ha produït un error desconegut. Probablement es degui a un error del propi "
"Bug Buddy. Informeu-ne manualment bugzilla.gnome.org\n"
"\n"

#: ../src/bug-buddy.c:803
#, c-format
msgid "Unable to create the bug report: %s\n"
msgstr "No s'ha pogut crear l'informe de l'error: %s\n"

#: ../src/bug-buddy.c:805
#, c-format
msgid "There was an error creating the bug report\n"
msgstr "S'ha produït un error en crear l'informe d'error\n"

#: ../src/bug-buddy.c:865
msgid "Sending…"
msgstr "S'està enviant…"

#: ../src/bug-buddy.c:1025
msgid ""
"The description you provided for the crash is very short. Are you sure you "
"want to send it?"
msgstr ""
"La descripció que heu proporcionat per a la fallada és molt curta. Segur que "
"voleu enviar-la?"

#: ../src/bug-buddy.c:1032
msgid ""
"A short description is probably not of much help to the developers "
"investigating your report. If you provide a better one, for instance "
"specifying a way to reproduce the crash, the issue can be more easily "
"resolved."
msgstr ""
"Una descripció curta segurament no servirà de massa per als desenvolupadors "
"que investiguin l'informe. Si en proporcioneu una de millor, per exemple "
"especificant una manera de reproduir l'errada, es podrà resoldre més "
"fàcilment l'errada."

#: ../src/bug-buddy.c:1040
msgid "_Review description"
msgstr "_Revisa la descripció"

#: ../src/bug-buddy.c:1049
msgid "_Send anyway"
msgstr "_Envia de totes maneres"

#. Translators: This is the hyperlink which takes to http://live.gnome.org/GettingTraces/DistroSpecificInstructions
#. * page. Please also mention that the page is in English
#: ../src/bug-buddy.c:1146
msgid "Getting useful crash reports"
msgstr "Com obtenir informes de fallada útils"

#: ../src/bug-buddy.c:1164
#, c-format
msgid ""
"The application %s crashed. The bug reporting tool was unable to collect "
"enough information about the crash to be useful to the developers.\n"
"\n"
"In order to submit useful reports, please consider installing debug packages "
"for your distribution.\n"
"Click the link below to get information about how to install these "
"packages:\n"
msgstr ""
"L'aplicació %s ha fallat. L'eina de recol·lecció d'errors no ha sigut capaç "
"de recollir prou informació sobre la fallada perquè pugui ser útil als "
"desenvolupadors.\n"
"\n"
"Per poder enviar informes útils, considereu la instal·lació dels paquets de "
"depuració de la vostra distribució.\n"
"Feu clic a l'enllaç de sota per obtenir més informació sobre com instal·lar "
"aquests paquets:\n"

#: ../src/bug-buddy.c:1202 ../src/bug-buddy.c:2091
msgid ""
"\n"
"\n"
"Please write your report in English, if possible."
msgstr ""
"\n"
"\n"
"Si és possible, redacteu el vostre informe en anglès."

#: ../src/bug-buddy.c:1215
#, c-format
msgid ""
"Information about the %s application crash has been successfully collected.  "
"Please provide some more details about what you were doing when the "
"application crashed.\n"
"\n"
"A valid email address is required.  This will allow the developers to "
"contact you for more information if necessary."
msgstr ""
"S'ha recollit correctament la informació sobre a la fallada de l'aplicació "
"%s. Proporcioneu algunes dades referides al que estàveu fent quan "
"l'aplicació ha fallat.\n"
"\n"
"Es necessita una adreça de correu vàlida. Això permetrà que els "
"desenvolupadors us contactin si els calgués més informació."

#: ../src/bug-buddy.c:1252
msgid "WARNING:"
msgstr "AVÍS:"

#: ../src/bug-buddy.c:1253
msgid ""
"Some sensitive data is likely present in the crash details.  Please review "
"and edit the information if you are concerned about transmitting passwords "
"or other sensitive data."
msgstr ""
"Pot ser que hi hagi dades sensibles en els detalls de la fallada. Reviseu i "
"editeu la informació si us preocupa transmetre contrasenyes i altres dades "
"sensibles."

#: ../src/bug-buddy.c:1290
msgid "Save File"
msgstr "Desa el fitxer"

#: ../src/bug-buddy.c:1310
msgid "-bugreport.txt"
msgstr "-informe.txt"

#: ../src/bug-buddy.c:1368
msgid "_Save Bug Report"
msgstr "_Desa l'informe d'error"

#: ../src/bug-buddy.c:1373
#, c-format
msgid ""
"The application %s has crashed.\n"
"Information about the crash has been successfully collected.\n"
"\n"
"This application is not known to Bug Buddy, therefore the bug report cannot "
"be sent to the GNOME Bugzilla.  Please save the bug to a text file and "
"report it to the appropriate bug tracker for this application."
msgstr ""
"Ha fallat l'aplicació %s.\n"
"S'han pogut recollir les dades de perquè ha fallat.\n"
"\n"
"El Bug Buddy no coneix l'aplicació que ha fallat; per tant, no es pot enviar "
"l'informe al Bugzilla del GNOME. Deseu aquest informe en un fitxer de text i "
"informeu el gestor de fallades d'aquesta aplicació."

#: ../src/bug-buddy.c:1462
#, c-format
msgid "There was an error displaying help: %s"
msgstr "S'ha produït un error en mostrar l'ajuda: %s"

#: ../src/bug-buddy.c:1923
msgid ""
"\n"
"\n"
"Bug Buddy is a utility that helps report debugging\n"
"information to the GNOME Bugzilla when a program crashes."
msgstr ""
"\n"
"\n"
"El Bug Buddy és una utilitat que ajuda a enviar\n"
"informació de depuració al Bugzilla del GNOME quan un programa falla."

#: ../src/bug-buddy.c:1975
msgid ""
"Bug Buddy could not load its user interface file.\n"
"Please make sure Bug Buddy was installed correctly."
msgstr ""
"El Bug Buddy no ha pogut carregar el seu fitxer d'interfície d'usuari.\n"
"Assegureu-vos que el Bug Buddy s'ha instal·lat correctament."

#: ../src/bug-buddy.c:1992
msgid "Collecting information from your system…"
msgstr "S'està recollint informació del vostre sistema…"

#: ../src/bug-buddy.c:1995
msgid "Either --appname or --package arguments are required.\n"
msgstr "Es necessita el paràmetre --appname o --package.\n"

#: ../src/bug-buddy.c:2001
msgid "Either --pid or --include arguments are required.\n"
msgstr "Es necessita el paràmetre --pid o --include.\n"

#: ../src/bug-buddy.c:2006
msgid "The --unlink-tempfile option needs an --include argument.\n"
msgstr "L'opció --unlink-tempfile necessita un argument --include.\n"

#: ../src/bug-buddy.c:2013
msgid ""
"Bug Buddy was unable to retrieve information regarding the version of GNOME "
"you are running.  This is most likely due to a missing installation of gnome-"
"desktop.\n"
msgstr ""
"El Bug Buddy no ha pogut obtenir informació quant a la versió del GNOME que "
"esteu executant. El més probable és que no tingueu instal·lat el gnome-"
"desktop.\n"

#: ../src/bug-buddy.c:2035
#, c-format
msgid ""
"The %s application has crashed.  We are collecting information about the "
"crash to send to the developers in order to fix the problem."
msgstr ""
"L'aplicació %s ha fallat. Estem recollint informació sobre la fallada per "
"enviar-la als desenvolupadors per poder corregir el problema."

#: ../src/bug-buddy.c:2052
msgid "Collecting information from the crash…"
msgstr "S'està recollint informació de la fallada…"

#: ../src/bug-buddy.c:2069
#, c-format
msgid ""
"Bug Buddy encountered the following error when trying to retrieve debugging "
"information: %s\n"
msgstr ""
"El Bug Buddy ha trobat el següent error quan intentava obtenir informació de "
"depuració: %s\n"

#: ../src/bug-buddy.c:2099
#, c-format
msgid ""
"Bug Buddy doesn't know how to send a suggestion for the application %s.\n"
msgstr "El Bug Buddy no sap com enviar un suggeriment per a l'aplicació %s.\n"

#: ../src/bug-buddy.c:2119
#, c-format
msgid ""
"Thank you for helping us to improve our software.\n"
"Please fill in your suggestions/error information for %s application.\n"
"\n"
"A valid email address is required. This will allow developers to contact you "
"for more information if necessary."
msgstr ""
"Gràcies per ajudar-nos a millorar el programari.\n"
"Empleneu la vostra informació d'error o els suggeriments per a l'aplicació "
"%s.\n"
"\n"
"Cal una adreça de correu vàlida. Això permetrà que els desenvolupadors es "
"posin en contacte amb vós en cas que els calgui més informació."

#: ../src/bug-buddy.c:2131
msgid "Suggestion / Error description:"
msgstr "Suggeriment / Descripció de l'error:"

#: ../src/bugzilla.c:413
#, c-format
msgid "HTTP Response returned bad status code %d"
msgstr "La resposta HTTP ha retornat un codi d'estat erroni (%d)"

#: ../src/bugzilla.c:429
#, c-format
msgid ""
"Unable to parse XML-RPC response\n"
"\n"
"%s"
msgstr ""
"No s'ha pogut analitzar la resposta XML-RPC\n"
"\n"
"%s"

#: ../src/bugzilla.c:461
#, c-format
msgid "Application does not track its bugs in the GNOME Bugzilla."
msgstr ""
"L'aplicació no fa el seguiment dels seus errors en el Bugzilla del GNOME."

#: ../src/bugzilla.c:467
#, c-format
msgid "Product or component not specified."
msgstr "No s'ha especificat el producte o component."

#: ../src/bugzilla.c:516
#, c-format
msgid "Unable to create XML-RPC message."
msgstr "No s'ha pogut crear el missatge XML-RPC."

#: ../src/gdb-buddy.c:50
msgid "gdb has already exited"
msgstr "gdb ja ha sortit"

#: ../src/gdb-buddy.c:91
msgid "Error on read; aborting."
msgstr "Error de lectura. S'està avortant."

#: ../src/gdb-buddy.c:259
#, c-format
msgid ""
"GDB could not be found on your system. Debugging information will not be "
"obtained."
msgstr ""
"No s'ha pogut trobar el GDB al sistema. No se n'obtindrà la informació de "
"depuració."

#: ../src/gdb-buddy.c:268
#, c-format
msgid ""
"Could not find the gdb-cmd file.\n"
"Please try reinstalling Bug Buddy."
msgstr ""
"No s'ha pogut trobar el fitxer gdb-cmd.\n"
"Intenteu tornar a instal·lar el Bug Buddy."

#: ../src/gdb-buddy.c:283
#, c-format
msgid ""
"There was an error running gdb:\n"
"\n"
"%s"
msgstr ""
"S'ha produït un error en executar el gdb:\n"
"\n"
"%s"

#~ msgid "MiniDump file with info about the crash"
#~ msgstr "Fitxer MiniDump amb informació sobre la fallada"

#~ msgid "The binary file could not be found. Try using an absolute path."
#~ msgstr ""
#~ "No s'ha pogut trobar el binari. S'està intentant utilitzar un camí "
#~ "absolut."

#~ msgid "Name of contact"
#~ msgstr "Nom del contacte"

#~ msgid "NAME"
#~ msgstr "Nom"

#~ msgid "Email address of contact"
#~ msgstr "Adreça electrònica de contacte"

#~ msgid "EMAIL"
#~ msgstr "Correu electrònic"

#~ msgid "Version of the package"
#~ msgstr "Versió del paquet"

#~ msgid "VERSION"
#~ msgstr "Versió"

#~ msgid "Core file from program"
#~ msgstr "Fitxer core del programa"

#~ msgid "PID of the program to kill after the report"
#~ msgstr "PID del programa que s'ha de matar després de l'informe"

#~ msgid "KILL"
#~ msgstr "Mata"

#~ msgid ""
#~ "<span weight=\"bold\">Network Connection Error</span>\n"
#~ "Maybe no Network Connection available.\n"
#~ "Do you want to store this report until a Network Connection is available?"
#~ msgstr ""
#~ "<span weight=\"bold\">Error en la connexió de xarxa</span>\n"
#~ "És possible que la connexió a la xarxa no estigui disponible.\n"
#~ "Voleu desar aquest informe fins que hi hagi una connexió a la xarxa "
#~ "disponible?"

#~ msgid ""
#~ "Your application has crashed. Information about the crash has been "
#~ "successfully collected.\n"
#~ "\n"
#~ "However we are working on GNOME debug server to handle correctly this "
#~ "information.\n"
#~ "\n"
#~ msgstr ""
#~ "El programa ha fallat. S'ha pogut recollir informació sobre la fallada.\n"
#~ "\n"
#~ "Tot i així estem treballant en un servidor de depuració per al GNOME per "
#~ "a gestionar correctament aquesta informació.\n"
#~ "\n"

#~ msgid "Unable to parse XML-RPC Response"
#~ msgstr "No s'ha pogut analitzar la resposta XML-RPC"

#~ msgid ""
#~ "Thank you for helping us improving our software.\n"
#~ "Please fill your suggestions/error information for %s application."
#~ msgstr ""
#~ "Gràcies per ajudar-nos a millorar el nostre programari.\n"
#~ "Empleneu la informació d'error i suggeriments per a l'aplicació %s."

#~ msgid "Done"
#~ msgstr "Fet"

#~ msgid "Show details"
#~ msgstr "Mostra els detalls"

#~ msgid "Couldn't load icon for %s"
#~ msgstr "No s'ha pogut carregar la icona per a %s"

#~ msgid "Bug-Buddy"
#~ msgstr "Bug-Buddy"

#~ msgid "Submit bug report"
#~ msgstr "Envia l'informe d'errors"

#~ msgid "Only send report to yourself"
#~ msgstr "Només envia l'informe d'error a vostè mateix"

#~ msgid "Save report to file"
#~ msgstr "Desa l'informe en un fitxer"

#~ msgid "core file"
#~ msgstr "fitxer core"

#~ msgid "nothing"
#~ msgstr "res"

#~ msgid "Could not find widget named %s at %s"
#~ msgstr "No s'ha trobat cap element d'interfície gràfica anomenat %s en %s"

#~ msgid ""
#~ "gdb has not finished getting the debugging information.\n"
#~ "Kill the gdb process (the stack trace will be incomplete)?"
#~ msgstr ""
#~ "El gdb no ha acabat d'obtenir la informació de depuració.\n"
#~ "Voleu interrompre el procés gdb? (la traça de la pila serà incompleta)"

#~ msgid "Save Backtrace"
#~ msgstr "Desa la traça"

#~ msgid "Please wait while Bug Buddy saves the stack trace..."
#~ msgstr "Espereu mentre el Bug Buddy desa la traça de la pila..."

#~ msgid ""
#~ "The stack trace was not saved in %s:\n"
#~ "\n"
#~ "%s\n"
#~ "\n"
#~ "Please try again, maybe with a different file name."
#~ msgstr ""
#~ "La traça de la pila no s'ha desat en %s:\n"
#~ "\n"
#~ "%s\n"
#~ "\n"
#~ "Proveu de nou, potser amb un altre nom de fitxer."

#~ msgid "_Start Mailer"
#~ msgstr "_Inicia el programa de correu"

#~ msgid "Hide Debugging Options"
#~ msgstr "Amaga les opcions de depuració"

#~ msgid "Show Debugging Options"
#~ msgstr "Mostra les opcions de depuració"

#~ msgid ""
#~ "$GNOME_CRASHED_APPNAME is deprecated.\n"
#~ "Please use the --appname command line argument instead."
#~ msgstr ""
#~ "$GNOME_CRASHED_APPNAME deixarà de fer-se servir.\n"
#~ "Utilitzeu en el seu lloc l'argument de la línia d'ordres --appname."

#~ msgid ""
#~ "$GNOME_CRASHED_PID is deprecated.\n"
#~ "Please use the --pid command line argument instead."
#~ msgstr ""
#~ "$GNOME_CRASHED_PID deixarà de fer-se servir.\n"
#~ "Utilitzeu en el seu lloc l'argument de la línia d'ordres --pid."

#~ msgid ""
#~ "To debug a process, the application name is also required.\n"
#~ "Please also supply an --appname command line argument."
#~ msgstr ""
#~ "Per a depurar un procés, es requereix també el nom de l'aplicació.\n"
#~ "Doneu també l'argument de la línia d'ordres --appname."

#~ msgid ""
#~ "Bug Buddy could not update its bug information.\n"
#~ "The old one will be used."
#~ msgstr ""
#~ "Bug Buddy no ha pogut actualitzar la seva informació sobre els errors.\n"
#~ "S'utilitzarà la informació antiga."

#~ msgid "<b>Description</b>"
#~ msgstr "<b>Descripció</b>"

#~ msgid "<b>Sendmail Settings</b>"
#~ msgstr "<b>Paràmetres del Sendmail</b>"

#~ msgid "<b>Summary</b>"
#~ msgstr "<b>Resum</b>"

#~ msgid ""
#~ "<span weight=\"bold\" size=\"larger\">Bug Buddy update</span>\n"
#~ "\n"
#~ "The bug information that is stored on your system is outdated. Choosing "
#~ "\"Update\" will update it. Choosing \"Don't update\" will force the bug "
#~ "reporting to use old data."
#~ msgstr ""
#~ "<span weight=\"bold\" size=\"larger\">Actualització de Bug Buddy</span>\n"
#~ "\n"
#~ "La informació sobre els errors que està desada en el vostre sistema està "
#~ "desactualitzada. Si seleccioneu «Actualitza» s'actualitzarà. Si "
#~ "seleccioneu «No actualitzis», es forçarà l'ús de dades antigues en "
#~ "l'informe."

#~ msgid "Advanced"
#~ msgstr "Avançat"

#~ msgid "Assbarn Phenomenon"
#~ msgstr "Assbarn Phenomenon"

#~ msgid "Binary file:"
#~ msgstr "Fitxer binari:"

#~ msgid ""
#~ "Bug Buddy can send debugging information with your bug report.\n"
#~ "The correct options should have been selected for you automatically."
#~ msgstr ""
#~ "El Bug Buddy pot enviar la informació de depuració amb l'informe "
#~ "d'error.\n"
#~ "Les opcions correctes s'hauríen d'haver seleccionat automàticament."

#~ msgid ""
#~ "Bug Buddy is now collecting information on your crash to submit to a bug "
#~ "tracking system. This is an automated process, and may take a few "
#~ "minutes. When it is done, you can press 'Show Debugging Details' to see "
#~ "the information or press 'Forward' to move to the next step in the "
#~ "process."
#~ msgstr ""
#~ "Bug Buddy està recollint informació sobre la fallada per a enviar-la a un "
#~ "sistema de seguiment d'errors. Aquest procés és automàtic, i pot trigar "
#~ "uns minuts. Quan finalitzi, podreu prémer «Mostra detalls de la "
#~ "depuració» per a veure la informació o «Següent» per a moure al següent "
#~ "pas del procés."

#~ msgid ""
#~ "Bug Buddy uses email to submit the bug reports.\n"
#~ "Please choose how you would like Bug Buddy to send email."
#~ msgstr ""
#~ "Bug Buddy utilitza el correu electrònic per a enviar els informes "
#~ "d'errors.\n"
#~ "Seleccioneu com voleu que s'enviïn."

#~ msgid "Cc:"
#~ msgstr "Cc:"

#~ msgid "Core file:"
#~ msgstr "Fitxer core:"

#~ msgid "D_ebug a crashed or running application (experts only)"
#~ msgstr ""
#~ "D_epura una aplicació que ha fallat o que s'està executant (només experts)"

#~ msgid "Debug:"
#~ msgstr "Depura:"

#~ msgid "Downloading Files"
#~ msgstr "S'estan descarregant els fitxers"

#~ msgid "Email:"
#~ msgstr "Adreça electrònica:"

#~ msgid "Include a text file:"
#~ msgstr "Inclou un fitxer de text:"

#~ msgid "Just s_ave to a file so I can submit a bug report manually"
#~ msgstr "Simplement des_a a un fitxer per a enviar un informe manualment"

#~ msgid "Name:"
#~ msgstr "Nom:"

#~ msgid "Path of sendmail:"
#~ msgstr "Camí complet del sendmail:"

#~ msgid "Please choose a component, version, and severity level."
#~ msgstr "Escolliu un component, versió, i nivell de gravetat."

#~ msgid ""
#~ "Please make any final corrections to the bug report.\n"
#~ "Notice that it will be shown at http://bugzilla.gnome.org\n"
#~ "It will include your name, e-mail address, and maybe\n"
#~ "some information about how the application crashed. If the\n"
#~ "document you were working on contained sensitive information,\n"
#~ "you may not want to submit this bug report"
#~ msgstr ""
#~ "Feu qualsevol correcció final a l'informe d'error.\n"
#~ "Tingueu en compte que es mostrarà a http://bugzilla.gnome.org/.\n"
#~ "Inclourà el vostre nom, adreça de correu electrònic i potser alguna "
#~ "informació\n"
#~ "sobre com va fallar l'aplicació. Si el document en el que estaveu\n"
#~ "treballant contenia informació privada, potser no hauríeu d'enviar\n"
#~ "aquest informe d'error"

#~ msgid ""
#~ "Please select the product or application for the bug you wish to report."
#~ msgstr ""
#~ "Seleccioneu el producte o aplicació a què es refereix l'error del què "
#~ "voleu informar."

#~ msgid ""
#~ "Please take a minute to see if your bug is one of the most frequently "
#~ "reported bugs.\n"
#~ "If the bug is already reported, please do not report it again."
#~ msgstr ""
#~ "Comproveu abans si el vostre error no és un dels que se n'informa més "
#~ "freqüentment.\n"
#~ "Si ja s'ha informat de l'error, és preferible que no el torneu a enviar."

#~ msgid ""
#~ "Please wait while Bug Buddy updates its list\n"
#~ "of products for the bug tracking systems."
#~ msgstr ""
#~ "Espereu mentre el Bug Buddy actualitza la seva llista\n"
#~ "de productes per als sistemes de seguiment d'errors."

#~ msgid "Process ID:"
#~ msgstr "Identificador del procés:"

#~ msgid "Save the report to..."
#~ msgstr "Desa l'informe a..."

#~ msgid "Saving: "
#~ msgstr "Desa:"

#~ msgid "Severity:"
#~ msgstr "Gravetat:"

#~ msgid "Show _Applications"
#~ msgstr "Mostra les _aplicacions"

#~ msgid "Show _Products"
#~ msgstr "Mostra els _productes"

#~ msgid "Show most frequent bugs in:"
#~ msgstr "Mostra els errors més freqüents en:"

#~ msgid "Simple"
#~ msgstr "Simple"

#~ msgid "Start"
#~ msgstr "Comença"

#~ msgid "Stop"
#~ msgstr "Atura"

#~ msgid "Submit another bug"
#~ msgstr "Envia un altre error"

#~ msgid "The _application does not function correctly"
#~ msgstr "L'_aplicació no funciona correctament"

#~ msgid "The _documentation is wrong"
#~ msgstr "La _documentació és incorrecta"

#~ msgid "The _translation is wrong"
#~ msgstr "La _traducció és incorrecta"

#~ msgid "To:"
#~ msgstr "A:"

#~ msgid "Use _sendmail directly"
#~ msgstr "Utilitza _sendmail directament"

#~ msgid "Version:"
#~ msgstr "Versió:"

#~ msgid ""
#~ "Welcome to Bug Buddy, a bug reporting tool for GNOME. It will step you "
#~ "through the process of submitting a bug report.\n"
#~ "\n"
#~ "Please select the kind of problem you want to report, and press the "
#~ "\"Forward\" button:"
#~ msgstr ""
#~ "Benvingut al Bug Buddy, una eina per a informar d'errors per al GNOME. "
#~ "Sereu guiats a través del procés d'enviament d'un informe d'error.\n"
#~ "\n"
#~ "Seleccioneu el tipus de problema del qual voleu informar i premeu el botó "
#~ "«Endavant»:"

#~ msgid "_Don't update"
#~ msgstr "_No actualitzis"

#~ msgid "_Request a missing feature"
#~ msgstr "_Demana una funcionalitat inexistent"

#~ msgid "_Update"
#~ msgstr "Act_ualitza"

#~ msgid "component"
#~ msgstr "component"

#~ msgid "desc"
#~ msgstr "desc"

#~ msgid "email info"
#~ msgstr "info del correu"

#~ msgid "finished"
#~ msgstr "finalitzat"

#~ msgid "gdb"
#~ msgstr "gdb"

#~ msgid "intro"
#~ msgstr "intro"

#~ msgid "mail config"
#~ msgstr "configuració del correu"

#~ msgid "mostfreq"
#~ msgstr "mésfreq"

#~ msgid "product"
#~ msgstr "producte"

#~ msgid "%d of %d"
#~ msgstr "%d de %d"

#~ msgid "ID"
#~ msgstr "ID"

#~ msgid "Product"
#~ msgstr "Producte"

#~ msgid "Component"
#~ msgstr "Component"

#~ msgid ""
#~ "Bug Buddy could not open '%s'.\n"
#~ "Please make sure Bug Buddy was installed correctly.\n"
#~ "\n"
#~ "Bug Buddy will now quit."
#~ msgstr ""
#~ "Bug Buddy no ha pogut obrir '%s'.\n"
#~ "Assegureu-vos que el Bug Buddy s'ha instal·lat correctament."

#~ msgid ""
#~ "Bug Buddy could not find any information on where to submit bugs.\n"
#~ "\n"
#~ "Please make sure Bug Buddy was installed correctly.\n"
#~ "\n"
#~ "Bug Buddy will now quit."
#~ msgstr ""
#~ "El Bug Buddy no ha pogut trobar informació sobre on enviar els informes "
#~ "d'error.\n"
#~ "\n"
#~ "Assegureu-vos que el Bug Buddy s'ha instal·lat correctament.\n"
#~ "\n"
#~ "El Bug Buddy sortirà ara."

#~ msgid "%s (Panel Applet)"
#~ msgstr "%s (Miniaplicació del quadre)"

#~ msgid "Application"
#~ msgstr "Aplicació"

#~ msgid "URI"
#~ msgstr "URI"

#~ msgid "URI to show when clicked."
#~ msgstr "URI a mostrar quan es fa clic."

#~ msgid "Visited"
#~ msgstr "Visitada"

#~ msgid "If the URI has been visited before."
#~ msgstr "Si s'ha visitat la URI anteriorment."

#~ msgid "Obtaining stack trace... (%d)"
#~ msgstr "S'està obtenint la traça de la pila... (%d)"

#~ msgid ""
#~ "Unable to process core file with gdb:\n"
#~ "'%s'"
#~ msgstr ""
#~ "No s'ha pogut processar el fitxer core amb el gdb:\n"
#~ "'%s'"

#~ msgid ""
#~ "GDB was unable to determine which binary created\n"
#~ "'%s'"
#~ msgstr ""
#~ "El GDB no ha pogut determinar quin és el binari que va crear\n"
#~ "'%s'"

#~ msgid "Both a binary file and PID are required to debug."
#~ msgstr "Es requereixen tant el fitxer binari com el PID per a depurar."

#~ msgid "Could not read symbolic link information for %s"
#~ msgstr "No s'ha pogut llegir la informació de l'enllaç simbòlic per a %s"

#~ msgid "The file has too many symbolic links."
#~ msgstr "El fitxer té massa enllaços simbòlics."

#~ msgid "Main loop isn't running!"
#~ msgstr "El bucle principal no s'està executant"

#~ msgid "Error setting up sigchld handler: %s"
#~ msgstr "S'ha produït un error en configurar el gestor de sigchld: %s"

#~ msgid "Invalid filename."
#~ msgstr "El nom del fitxer no és vàlid."

#~ msgid ""
#~ "There already exists a file name '%s'.\n"
#~ "\n"
#~ "Do you wish to overwrite this file?"
#~ msgstr ""
#~ "El fitxer «%s» ja existeix.\n"
#~ "\n"
#~ "Voleu sobrescriure aquest fitxer?"

#~ msgid "_Overwrite"
#~ msgstr "S_obreescriure"

#~ msgid "Could not create a backup file."
#~ msgstr "No s'ha pogut crear una còpia de seguretat."

#~ msgid "Welcome to Bug Buddy"
#~ msgstr "Benvingut al Bug Buddy"

#~ msgid "Collecting debug info"
#~ msgstr "S'està reunint informació de depuració"

#~ msgid "Select a Product or Application"
#~ msgstr "Seleccioneu un producte o aplicació"

#~ msgid "Select a Component"
#~ msgstr "Seleccioneu un component"

#~ msgid "Frequently Reported Bugs"
#~ msgstr "Errors dels que s'informa freqüentment"

#~ msgid "Bug Description"
#~ msgstr "Descripció de l'error"

#~ msgid "Mail Configuration"
#~ msgstr "Configuració del correu"

#~ msgid "Confirmation"
#~ msgstr "Confirmació"

#~ msgid "Finished!"
#~ msgstr "Finalitzat!"

#~ msgid "translator_credits-PLEASE_ADD_YOURSELF_HERE"
#~ msgstr ""
#~ "Softcatalà <tradgnome@softcatala.org>\n"
#~ "Jordi Mallach <jordi@sindominio.net>"

#~ msgid "The graphical bug reporting tool for GNOME."
#~ msgstr "L'eina gràfica del GNOME per a informar d'errors."

#~ msgid "Please enter your name."
#~ msgstr "Introduïu el vostre nom."

#~ msgid "Please enter a valid email address."
#~ msgstr "Introduïu una adreça electrònica vàlida."

#~ msgid ""
#~ "'%s' doesn't seem to exist.\n"
#~ "\n"
#~ "Please check the path to sendmail again."
#~ msgstr ""
#~ " No s'ha trobat '%s'.\n"
#~ "\n"
#~ "Comproveu el camí a sendmail de nou."

#~ msgid "Please enter a valid email command."
#~ msgstr "Introduïu una ordre de correu vàlida."

#~ msgid "The specified file does not exist."
#~ msgstr "El fitxer especificat no existeix."

#~ msgid "File is of type: %s"
#~ msgstr "El fitxer és del tipus: %s"

#~ msgid ""
#~ "'%s' is a %s file.\n"
#~ "\n"
#~ "Bug Buddy can only submit plaintext (text/*) files."
#~ msgstr ""
#~ "'%s' és un fitxer %s.\n"
#~ "\n"
#~ "Bug Buddy només pot enviar fitxers de text simple (text/*)."

#~ msgid "You must include a comprehensible subject line in your bug report."
#~ msgstr ""
#~ "A l'informe d'error heu d'incloure una línia d'assumpte explicativa."

#~ msgid "You must include a comprehensible description in your bug report."
#~ msgstr "A l'informe d'error heu d'incloure una descripció exhaustiva."

#~ msgid "Please wait while Bug Buddy saves your bug report..."
#~ msgstr "Espereu mentre el Bug Buddy desa el vostre informe d'error..."

#~ msgid ""
#~ "The bug report was not saved in %s:\n"
#~ "\n"
#~ "%s\n"
#~ "\n"
#~ "Please try again, maybe with a different file name."
#~ msgstr ""
#~ "L'informe d'error no s'ha desat en %s:\n"
#~ "\n"
#~ "%s\n"
#~ "\n"
#~ "Proveu de nou, potser amb un nom de fitxer diferent."

#~ msgid "Your bug report was saved in %s"
#~ msgstr "S'ha desat el vostre informe d'error a %s"

#~ msgid "Please wait while Bug Buddy submits your bug report..."
#~ msgstr "Espereu mentre el Bug Buddy envia el vostre informe d'error..."

#~ msgid ""
#~ "Your bug report has been submitted to:\n"
#~ "\n"
#~ "        <%s>\n"
#~ "\n"
#~ "Bug reporting is an important part of making Free Software. Thank you for "
#~ "helping."
#~ msgstr ""
#~ "El vostre informe d'error s'ha enviat a:\n"
#~ "\n"
#~ "        <%s>\n"
#~ "\n"
#~ "Informar dels errors és una part important del procés de creació de "
#~ "programari lliure. Gràcies per la vostra ajuda."

#~ msgid "Please, select the core file or running application to debug."
#~ msgstr ""
#~ "Seleccioneu el fitxer de volcat «core» o una aplicació en execució per a "
#~ "depurar."

#~ msgid ""
#~ "%s\n"
#~ "\n"
#~ "Notice that the software you are running (%s) is older than six months.\n"
#~ "You may want to consider upgrading to a newer version of %s system"
#~ msgstr ""
#~ "%s\n"
#~ "\n"
#~ "Tingueu en compte que el programari que esteu executant (%s) té més de "
#~ "sis mesos.\n"
#~ "Pot ser voleu actualitzar a una versió nova del sistema %s"

#~ msgid "Please choose a product for your bug report."
#~ msgstr "Seleccioneu el producte per a l'informe d'error."

#~ msgid ""
#~ "Please choose a component, version, and severity level for product %s"
#~ msgstr ""
#~ "Escolliu un component, versió, i nivell de gravetat per al producte %s"

#~ msgid "Please choose an application for your bug report."
#~ msgstr "Seleccioneu una aplicació per a l'informe d'error."

#~ msgid ""
#~ "Please choose a component, version, and severity level for application %s"
#~ msgstr ""
#~ "Escolliu un component, versió, i nivell de gravetat per a l'aplicació %s"

#~ msgid ""
#~ "This Application has bug information, but Bug Buddy doesn't know about "
#~ "it. Please choose another application."
#~ msgstr ""
#~ "Aquesta aplicació té informació d'error, però Bug Buddy no sap res "
#~ "d'ella. Seleccioneu una altra aplicació."

#~ msgid "You must specify a component for your bug report."
#~ msgstr "Heu d'indicar el component a què es refereix l'informe d'error."

#~ msgid "Last time Bug Buddy checked for updates"
#~ msgstr "Última vegada que Bug Buddy va comprovar si hi ha actualitzacions"

#~ msgid ""
#~ "Path in your local filesystem where sendmail or equivalent is located."
#~ msgstr ""
#~ "Camí al vostre sistema de fitxers local on es troba sendmail o un "
#~ "d'equivalent."

#~ msgid "Path to sendmail like mailer program"
#~ msgstr "Camí al programa de correu semblant a sendmail"

#~ msgid ""
#~ "This is the last time (UNIX timestamp) when Bug Buddy checked for updates "
#~ "of Bugzilla information."
#~ msgstr ""
#~ "Aquesta és l'última vegada (marca de temps UNIX) que Bug Buddy va "
#~ "comprovar si hi havien actualitzacions de la informació de Bugzilla."

#~ msgid ""
#~ "Use sendmail for submitting bug reports to GNOME Bugzilla. Right now, "
#~ "this is the only supported method."
#~ msgstr ""
#~ "Utilitza sendmail per a enviar els informes d'error al Bugzilla del "
#~ "GNOME. És l'únic mètode suportat per ara."

#~ msgid "*"
#~ msgstr "*"
