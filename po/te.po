# translation of te.po to Telugu
# Telugu translation of bug-buddy.
# Copyright (C) Swecha Telugu Localisation Team <localisation@swecha.org>
# This file is distributed under the same license as the bug-buddy package.
#
# Suryan Narayana <suri_sun88@yahoo.co.in>, 2007.
# Krishna Babu K <kkrothap@redhat.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: te\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=bug-buddy&amp;component=general\n"
"POT-Creation-Date: 2008-11-30 17:19+0000\n"
"PO-Revision-Date: 2009-03-09 11:59+0530\n"
"Last-Translator: Krishna Babu K <kkrothap@redhat.com>\n"
"Language-Team: Telugu <en@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n\n"

#: ../data/bug-buddy.gtkbuilder.h:1
msgid "<b>What _were you doing when the application crashed?</b>"
msgstr "<b>మీరు కార్యక్షేత్రము కుప్పకూలిన సమయములలో ఏమి చెస్తున్నారు? (_w)</b>"

#: ../data/bug-buddy.gtkbuilder.h:2
msgid "<b>Your _email address:</b> "
msgstr "<b>మీ  ఎలక్ట్రానిక్   తపాల   యొక్క  చిరునామా:(_e)</b> "

#: ../data/bug-buddy.gtkbuilder.h:3
msgid ""
"<small><i><b>Note:</b>  Sensitive information may be present in the crash "
"details.  Please review the crash details if you are concerned about "
"transmitting passwords or other sensitive information.</i></small>"
msgstr ""
"<small><i><b>గమనిక:</b> కుప్పకూలిన విషయములలో కొంత స్పందిత వివరము కూడా ఉన్నది. "
"రహస్యపదాన్ని ప్రసారణ చేయుటకు సంబందించిన విషయము గురించి గాని లేదా  స్పందిత వివరము "
"గురించి గాని ఐతే  దయ చేసి ఉపదర్శనం చేసి విషయమునను  సవరించండి.</i></small>"

#: ../data/bug-buddy.gtkbuilder.h:4
msgid "<span size=\"xx-large\"><b>Bug reporting tool</b></span>"
msgstr "<span size=\"xx-large\"><b>లోపమును నివేదించు పనిముట్టు</b></span>"

#: ../data/bug-buddy.gtkbuilder.h:5 ../src/bug-buddy.c:1931
msgid "Bug Buddy"
msgstr "Bug Buddy"

#: ../data/bug-buddy.gtkbuilder.h:6
msgid "C_opy"
msgstr "నకలుతీయి (_o)"

#: ../data/bug-buddy.gtkbuilder.h:7
msgid "Review Crash Details"
msgstr "కుప్పకూలిన   వివరలములను   పరిశీలించడం"

#: ../data/bug-buddy.gtkbuilder.h:8
msgid "Send _other pending crash reports"
msgstr "నిల్వలో ఉన్న   మిగిలిన  కుప్పకూలిన  వివరములను  పంపడం (_o)"

#: ../data/bug-buddy.gtkbuilder.h:9
msgid "_Review Crash Details"
msgstr "కుప్పకూలిన   వివరలములను   పరిశీలించడం (_R)"

#: ../data/bug-buddy.gtkbuilder.h:10
msgid "_Send"
msgstr "పంపుట (_S)"

#: ../data/bug-buddy.desktop.in.in.h:1
msgid "Bug Report Tool"
msgstr "లోపపు   నివేదన   యొక్క   పనిముట్టు"

#: ../data/bug-buddy.desktop.in.in.h:2
msgid "Report a bug in GNOME-based applications"
msgstr "GNOME యొక్క   కార్యక్షేత్రములలో  లోపమును  నివేదించండి."

#: ../data/bug-buddy.schemas.in.h:1
msgid "Bug reporter name"
msgstr "లోపమును  నివేదించువారి  పేరు"

#: ../data/bug-buddy.schemas.in.h:2
msgid "Email Address"
msgstr "ఇ-తపాలా  యొక్క చిరునామా"

#: ../data/bug-buddy.schemas.in.h:3
msgid ""
"Email Address for submitting bug reports to GNOME Bugzilla. This address "
"will be used for correspondence regarding the bug you are submitting. If you "
"already have a GNOME Bugzilla account, please use it as your Email Address."
msgstr ""
"GNOME Bugzilla కు లోపపు నివేదనలను పంపుటకు ఇ-తపాలా  యొక్క చిరునామా ఇవ్వండి.ఈ చిరునామా మీరు "
"పంపే లోపము  గురించిన దానికి ఉపయోగపడుతుంది.మీకు  GNOME Bugzilla లో ఖాతా ఉంటే ,దానిని  మీ   "
"ఎలక్ట్రానిక్   తపాల   యొక్క  చిరునామా గా ఉపయోగించండి."

#: ../data/bug-buddy.schemas.in.h:4
msgid "File to store unsent bug reports."
msgstr "అమర్పుతీసివేసిన బగ్ నివేదికలను నిల్వవుంచుటకు దస్త్రము"

#: ../data/bug-buddy.schemas.in.h:5
msgid ""
"File where bug-buddy will save your bug report in case it can't be sent "
"immediately to Bugzilla."
msgstr ""
"bug-buddy అనునది మీ బగ్ నివేదికను తక్షణమే బగ్‌జిల్లాకు పంపలేకపోతే దానిని నిల్వచేయు "
"దస్త్రము"

#: ../data/bug-buddy.schemas.in.h:6
msgid "GTK+ module for crash collecting support."
msgstr "క్రాష్ సేకరింపు మద్దతుకొరకు GTK+ మాడ్యూల్."

#: ../data/bug-buddy.schemas.in.h:7
msgid "Real name of user reporting the bug."
msgstr "లోపపు  నివేదనలను  నివేదించు  వారి  అసలు  పేరు"

#: ../data/bug-buddy.schemas.in.h:8
msgid "This key determines the GTK+ module to load for crash collecting support."
msgstr "క్రాష్ సేకరణ మద్దతు కొరకు లోడగుటకు ఈ కీ GTK+ మాడ్యూల్‌ను నిర్దారిస్తుంది."

#: ../src/bug-buddy.c:82
msgid "GNOME bug-buddy"
msgstr "GNOME bug-buddy"

#: ../src/bug-buddy.c:90
msgid "Package containing the program"
msgstr "కార్యక్రమం  యొక్క  కట్ట"

#: ../src/bug-buddy.c:90
msgid "PACKAGE"
msgstr "PACKAGE"

#: ../src/bug-buddy.c:91
msgid "File name of crashed program"
msgstr "కుప్పకూలిన  కార్యక్రమము  యొక్క  నామము"

#: ../src/bug-buddy.c:91 ../src/bug-buddy.c:93 ../src/bug-buddy.c:95
msgid "FILE"
msgstr "FILE"

#: ../src/bug-buddy.c:92
msgid "PID of crashed program"
msgstr "కుప్పకూలిన  కార్యక్రమము  యొక్క PID"

#: ../src/bug-buddy.c:92
msgid "PID"
msgstr "PID"

#: ../src/bug-buddy.c:93
msgid "Text file to include in the report"
msgstr "నివేదనలో చేర్చవలసిన  పాత్యపు  దస్త్రం"

#: ../src/bug-buddy.c:94
msgid "Delete the included file after reporting"
msgstr "చేర్చిన దస్త్రాన్ని నివేదన తర్వాత తొలగించుము"

#: ../src/bug-buddy.c:95
msgid "MiniDump file with info about the crash"
msgstr "కుప్పకూలుటకు సంభందిచిన సమాచారముతో మినీడంప్ దస్త్రము"

#: ../src/bug-buddy.c:377
msgid "Copy _Link Address"
msgstr "లింకు చిరునామాను నకలుతీయి (_L)"

#: ../src/bug-buddy.c:426
#, c-format
msgid "Bug Buddy was unable to view the link \"%s\"\n"
msgstr "Bug buddy జోడింపుని  \"%s\" కనిపెట్టలేకపోయింది\n"

#: ../src/bug-buddy.c:461
msgid ""
"There has been a network error while sending the report. Do you want to save "
"this report and send it later?"
msgstr ""
"నివేదికను పంపుచున్నప్పుడు అక్కడ నెట్వర్కు దోషము వుంది. మీరు ఈ నివేదికను దాచి మరలా "
"పంపుదామని అనుకొనుచున్నారా?"

#: ../src/bug-buddy.c:464
msgid "Please ensure that your Internet connection is active and working correctly."
msgstr ""
"మీ ఇంటర్నెట్ అనుసంధానం క్రియాశీలమైవుండునట్లు మరియు సరిగా పనిచేయునట్లు "
"దయచేసి చూసుకోండి."

#: ../src/bug-buddy.c:551 ../src/bug-buddy.c:1218
msgid "_Close"
msgstr "మూత (_C)"

#: ../src/bug-buddy.c:595
#, c-format
msgid ""
"A bug report detailing your software crash has been sent to GNOME. This "
"information will allow the developers to understand the cause of the crash "
"and prepare a solution for it.\n"
"\n"
"You may be contacted by a GNOME developer if more details are required about "
"the crash.\n"
"\n"
"You can view your bug report and follow its progress with this URL:\n"
msgstr ""
"సాఫ్ట్వేర్  యొక్క  నష్టము గురించిన  నివెదనన   GNOME కు  పంపబడిం ది .ఈ  విషయము  వల్ల  "
"తయారుచెయువారికి  ఒక  సమాధానం దొరుకుతుంది.\n"
"\n"
"మీకు సాఫ్ట్వేర్  యొక్క  నష్టము  గురించి తెలుసుకోవాలంటే  Gnome తయారుదారుడుని సంపర్కం చేయవచ్ఛు.\n"
"\n"
"సాఫ్ట్వేర్  యొక్క  నష్టము గురించిన  నివెదనను గమనించి  దాని  పురొగమునను  యు ఆర్ ఎల్ సార్వజనీక సమాచార "
"సేకరణ   ద్వారా తెలుసుకొనవచ్చు.\n"

#: ../src/bug-buddy.c:606
msgid ""
"Bug Buddy has encountered an error while submitting your report to the "
"Bugzilla server.  Details of the error are included below.\n"
"\n"
msgstr ""
"Bug buddy మీ  నివేదనను   Bugzilla server కు  పంపించినప్పుడు  దోషమును  చూపినది . దోషము  "
"యొక్క  వివరములు  క్రింద  తెలుపబడి  ఉన్నవి.\n"
"\n"

#: ../src/bug-buddy.c:612
#, c-format
msgid ""
"Bugzilla reported an error when trying to process your request, but was "
"unable to parse the response."
msgstr ""
"మీ  కోరికను    క్రమణం   చేస్తుంటే  బగ్జిల్లా  ఒక  తప్పును నివేదించింది  కానీ , స్పందనను పార్స్    "
"చేయలేకపొయింది ."

#: ../src/bug-buddy.c:615
#, c-format
msgid "The email address you provided is not valid."
msgstr "మీరు  ఇచ్చిన  ఎలక్ర్టానిక్  తపాలా  యొక్క  చిరునామ  చెల్లదు."

#: ../src/bug-buddy.c:617
#, c-format
msgid "The account associated with the email address provided has been disabled."
msgstr "మీరు   ఇచ్చిన   ఎలక్ర్టానిక్  తపాలా   యొక్క   చిరునామ  యొక్క  ఖాతా  నిరుపయోగము   చేయబదినది."

#: ../src/bug-buddy.c:620
#, c-format
msgid ""
"The product specified doesn't exist or has been renamed.  Please upgrade to "
"the latest version."
msgstr ""
"మీ  నిర్దేశిత   వస్తువు  లేదు , లేదా  దాని   యొక్క  పేరు  మార్చబదినది.దయచేసి   వివరణమును   "
"మెరుగుపరుచుకోండి"

#: ../src/bug-buddy.c:623
#, c-format
msgid ""
"The component specified doesn't exist or has been renamed.  Please upgrade "
"to the latest version."
msgstr ""
"మీ  నిర్దేశిత   వస్తువు   లేదు , లేదా  దాని   యొక్క  పేరు  మార్చబదినది.దయచేసి   వివరణమును   "
"మెరుగుపరుచుకోండి"

#: ../src/bug-buddy.c:626
#, c-format
msgid ""
"The summary is required in your bug report. This should not happen with the "
"latest Bug Buddy."
msgstr "మీ  లోపపు   నివెదనలో  సంక్షిప్తమైన   వివరము అవసరము.ఇది   నూతన   bug buddy తో  జరుగకూడదు."

#: ../src/bug-buddy.c:629
#, c-format
msgid ""
"The description is required in your bug report. This should not happen with "
"the latest Bug Buddy."
msgstr "మీ   లోపపు    నివెదనలో   వివరణ    ఇవ్వబడింది.ఇది   నూతన   bug buddy తో  జరుగకూడదు."

#: ../src/bug-buddy.c:632
#, c-format
msgid ""
"The fault code returned by Bugzilla is not recognized. Please report the "
"following information to bugzilla.gnome.org manually:\n"
"\n"
"%s"
msgstr ""
"మీరు  ఇచ్చిన   ప్రమేయ సంకేతము   bugzilla చే  గుర్తించబడ లేదు. దయ చేసి   క్రింది  విషయమును  "
"bugzilla.gnome.org కు  నివేదనను పంపగలరు. \n"
"\n"
"%s"

#: ../src/bug-buddy.c:639
#, c-format
msgid ""
"Server returned bad state.  This is most likely a server issue and should be "
"reported to bugmaster@gnome.org\n"
"\n"
"%s"
msgstr ""
"సేవిక    చెడు   స్థితిని  తెలుపుతున్నది . ఇది   సేవిక  యొక్క   విషయము  కావున  దీనిని  "
"bugmaster@gnome.orgకు  తెలుపగలరు.\n"
"\n"
"%s"

#: ../src/bug-buddy.c:644
#, c-format
msgid ""
"Failed to parse the xml-rpc response.  Response follows:\n"
"\n"
"%s"
msgstr ""
"XML-RPC యొక్క   స్పందనను పార్స్    చేయలేకపొయింది . స్పందన  ఈ  క్రింది   విధముగా  ఉన్నది:\n"
"\n"
"%s"

#: ../src/bug-buddy.c:648
#, c-format
msgid ""
"An unknown error occurred.  This is most likely a problem with bug-buddy. "
"Please report this problem manually at bugzilla.gnome.org\n"
"\n"
msgstr ""
"అనుకోని   దోషము  జరిగింది.ఇది  లోపపు   బడ్డి   తో  వచ్చిన  సమస్య  కావచ్చు.దయ చేసి ఈ  సమస్యను   "
"bugzilla.gnome.org కు  నివేదనను  పంపగలరు.\n"
"\n"

#: ../src/bug-buddy.c:804
#, c-format
msgid "Unable to create the bug report: %s\n"
msgstr "లోపపు    నివేదనను   సృష్టించలేకపొవుచున్నది: %s\n"

#: ../src/bug-buddy.c:806
#, c-format
msgid "There was an error creating the bug report\n"
msgstr "లోపపు    నివేదనను   సృష్టించటలో   దోషము   ఉన్నది\n"

#: ../src/bug-buddy.c:866
msgid "Sending..."
msgstr "పంపుచున్నది..."

#: ../src/bug-buddy.c:1026
msgid ""
"The description you provided for the crash is very short. Are you sure you "
"want to send it?"
msgstr ""
"కుప్పకూలుట గురించి మీరు అందిచినటువంటి వివరణ చాలా తక్కువ. మీరు ఖచ్చితంగా దీనినే "
"పంపాలని అనుకొనుచున్నారా?"

#: ../src/bug-buddy.c:1033
msgid ""
"A short description is probably not of much help to the developers "
"investigating your report. If you provide a better one, for instance "
"specifying a way to reproduce the crash, the issue can be more easily "
"resolved."
msgstr ""
"అభివృద్దికారులకు మీ నివేదికను విచారించుటకు కొద్దిమాత్రపు వివరణ అంతగా సహాయపడదు. "
"మీరు యింకొంత సోదాహరణముగా వివరించితే అంటే, క్రాష్ మరలాచేయవలెనంటే యెలాగో "
"తెలిపితే, సమస్య సులువుగా పరిష్కరించబడుతుంది."

#: ../src/bug-buddy.c:1041
msgid "_Review description"
msgstr "పునఃదర్శన వివరణ (_R)"

#: ../src/bug-buddy.c:1050
msgid "_Send anyway"
msgstr "ఏమైనాసరే పంపు (_S)"

#. Translators: This is the hyperlink which takes to http://live.gnome.org/GettingTraces/DistroSpecificInstructions
#. * page. Please also mention that the page is in English
#: ../src/bug-buddy.c:1147
msgid "Getting useful crash reports"
msgstr "ఉపయోగపడే   కుప్పకూలిన   నివేదనలను  రాబట్టు చున్నది."

#: ../src/bug-buddy.c:1198
#, c-format
msgid ""
"The application %s crashed. The bug reporting tool was unable to collect "
"enough information about the crash to be useful to the developers.\n"
"\n"
"In order to submit useful reports, please consider installing debug packages "
"for your distribution.\n"
"Click the link below to get information about how to install these "
"packages:\n"
msgstr ""
"కార్యక్షేత్రం %s కుప్పకూలినది. దోష నివేదనను తెలిపే పనిముట్టు తయారుదారులకు ఉపయోగపడేంత విషయమునను "
"సేకరించలేకపొయింది.\n"
"\n"
"ఉపయోగపడే   నివేదనలను  పంపుటకు ,దయ చేసి  debug packages లను  సంపూర్ణ ము  చెయ్యండి.\n"
"ఈ debug packages లను  సంపూర్ణ ము   చేయుటకు  దయ   చేసి  ఈ  క్రింది  జోడింపు ను   "
"నొక్కండి:\n"

#: ../src/bug-buddy.c:1236 ../src/bug-buddy.c:2061
msgid ""
"\n"
"\n"
"Please write your report in English, if possible."
msgstr ""
"\n"
"\n"
"మీకు   కుదిరితే    నివేదనను   ఆంగ్లములో   దయ  చేసి   వ్రాయండి."

#: ../src/bug-buddy.c:1249
#, c-format
msgid ""
"Information about the %s application crash has been successfully collected.  "
"Please provide some more details about what you were doing when the "
"application crashed.\n"
"\n"
"A valid email address is required.  This will allow the developers to "
"contact you for more information if necessary."
msgstr ""
"కుప్పకూలిన కార్యక్షేత్రం %s గురించి వివరాలు సేకరించబడినాయి. కార్యక్షేత్రం  కుప్పకూలిన  సమయములో   ఏమి  "
"చేయుచున్నారో  మరి కొన్ని  వివరములు   దయ చేసి  తెలుపగలరు.\n"
"\n"
"చెల్లదగు ఎలక్ర్టానిక్ తపాలా యొక్క చిరునామా ఇవ్వగలరు.మరిన్ని  కావలసిన వివరములకు   మిమ్మల్ని  సంపర్కం  "
"చేయుటకు ఇది  దోహద పడుతుంది ."

#: ../src/bug-buddy.c:1286
msgid "WARNING:"
msgstr "హెచ్చరిక:"

#: ../src/bug-buddy.c:1287
msgid ""
"Some sensitive data is likely present in the crash details.  Please review "
"and edit the information if you are concerned about transmitting passwords "
"or other sensitive data."
msgstr ""
"కుప్పకూలిన  విషయములలో  కొంత  స్పందిత  వివరము  కూడా  ఉన్నది.రహస్యపదాన్ని  ప్రసారణ  చేయుటకు  "
"సంబందించిన విషయము గురించి గాని లేదా    స్పందిత  వివరము   గురించి  గాని  ఐతే   దయ  చేసి ఉపదర్శనం  "
"చేసి  విషయమునను  సవరించండి."

#: ../src/bug-buddy.c:1324
msgid "Save File"
msgstr "దస్త్రమును దాయి"

#: ../src/bug-buddy.c:1344
msgid "-bugreport.txt"
msgstr "-bugreport.txt"

#: ../src/bug-buddy.c:1406
msgid "_Save Bug Report"
msgstr "లోపపు   నివెదనను   దాచుట  (_S)"

#: ../src/bug-buddy.c:1411
#, c-format
msgid ""
"The application %s has crashed.\n"
"Information about the crash has been successfully collected.\n"
"\n"
"This application is not known to bug-buddy, therefore the bug report cannot "
"be sent to the GNOME Bugzilla.  Please save the bug to a text file and "
"report it to the appropriate bug tracker for this application."
msgstr ""
"అనువర్తనము %s కుప్పకూలినది\n"
"కుప్పకూలుట గురించిన సమాచారము సమర్ధవంతంగా సేకరించబడింది.\n"
"\n"
"అనువర్తనము bug-buddyకి తెలియదు,కావున బగ్ నివేదనను "
"GNOME Bugzillaకు  పంపలేము.దయ చేసి లోపమునను పాఠముగా "
"నిలువ చేసి సరైన పట్టాదారుడకి ఈ అనువర్తనం కోసం నివేదనను పంపండి."

#: ../src/bug-buddy.c:1500
#, c-format
msgid "There was an error displaying help: %s"
msgstr "సహాయమును   చూపించుటలో  లోపము ఉన్నది: %s"

#: ../src/bug-buddy.c:1912
msgid ""
"\n"
"\n"
"Bug Buddy is a utility that helps report debugging\n"
"information to the GNOME Bugzilla when a program crashes."
msgstr ""
"\n"
"\n"
"bug buddy లోపపు  నిర్మూలనను  నివేదించు  ఒక   పరికరం\n"
"ఒక   కార్యక్రమం  కుప్పకూలినప్పుడు  GNOME Bugzilla కు  వివరిస్తుంది."

#: ../src/bug-buddy.c:1944
msgid ""
"Bug Buddy could not load its user interface file.\n"
"Please make sure Bug Buddy was installed correctly."
msgstr ""
"  bug buddy సంవిధాన   దస్త్రమును  దిగుమతి    చేయలేకపోయింది. దయ చేసి  bug buddy "
"ప్రతిష్టించబడినదో  లేదో చూడండి."

#: ../src/bug-buddy.c:1961
msgid "Collecting information from your system..."
msgstr "మీ  వ్యవస్థ   నుంచి  వివరములు    సేకరించుచున్నది..."

#: ../src/bug-buddy.c:1964
msgid "Either --appname or --package arguments are required.\n"
msgstr "--appname గాని  --package గాని ఆర్గుమెంట్లు  కావలనెను. \n"

#: ../src/bug-buddy.c:1971
msgid "Either --pid , --include or --minidump arguments are required.\n"
msgstr "--pid గాని, --include గాని లేదా --minidump గాని అర్గుమెంట్లు కావాలి.\n"

#: ../src/bug-buddy.c:1976
msgid "The --unlink-tempfile option needs a --include argument.\n"
msgstr "--unlink-tempfile ఐచ్చికానికి --include ఆర్గుమెంటు కావాలి.\n"

#: ../src/bug-buddy.c:1983
msgid ""
"Bug Buddy was unable to retrieve information regarding the version of GNOME "
"you are running.  This is most likely due to a missing installation of gnome-"
"desktop.\n"
msgstr ""
"మీ రు   ఉపయొగిస్తున్న GNOME యొక్క   వివరణము    గురించి   Bug Buddy వివరములు   "
"సేకరించలేకపొయింది .ఇది చాలా వరకు gnome యొక్క  వదిలివెసిన  దిగుమతి   వల్ల  జరిగింది.\n"

#: ../src/bug-buddy.c:2005
#, c-format
msgid ""
"The %s application has crashed.  We are collecting information about the "
"crash to send to the developers in order to fix the problem."
msgstr ""
"%s కార్యక్షేత్రం  కుప్పకూలినది. మేము  ఈ  కుప్పకూలుట   గురించిన    వివరాలు   సేకరించి ,"
"తయారుదారులకు సమస్యను గుర్తించే విధముగా    పంపుతున్నాము. "

#: ../src/bug-buddy.c:2022
msgid "Collecting information from the crash..."
msgstr "కుప్పకూలిన  దాని  నుంచి   విషయములు   సేకరించుచున్నాము."

#: ../src/bug-buddy.c:2039
#, c-format
msgid ""
"Bug Buddy encountered the following error when trying to retrieve debugging "
"information: %s\n"
msgstr "లోపపు   నిర్మూలనను  చేయు  సమయములో  bug buddy కి ఈ  క్రింది  దోషము  ఏర్పడినది : %s\n"

#: ../src/bug-buddy.c:2069
#, c-format
msgid "Bug Buddy doesn't know how to send a suggestion for the application %s.\n"
msgstr "%s కార్యక్షేత్రముకు   ఎలా   సూచన    పంపాలో bug buddy కి   తెలియడంలేదు.\n"

#: ../src/bug-buddy.c:2089
#, c-format
msgid ""
"Thank you for helping us improving our software.\n"
"Please fill your suggestions/error information for %s application.\n"
"\n"
"A valid email address is required.  This will allow developers to contact "
"you for more information if necessary."
msgstr ""
"మీ  సాఫ్ట్వేర్  ను   మెరుగుపరుచుకున్నందుకు ధన్యవాదములు.\n"
"%s కార్యక్షేత్రము లో లోపముల యొక్క వివరములను ,సలహాలను తెలియజేయండి.\n"
"\n"
"చెల్లదగు ఎలక్ర్టానిక్ తపాలా యొక్క చిరునామా ఇవ్వగలరు.మరిన్ని  కావలసిన వివరములకు   మిమ్మల్ని  సంపర్కం  "
"చేయుటకు ఇది  దోహద  పడుతుంది "

#: ../src/bug-buddy.c:2101
msgid "Suggestion / Error description:"
msgstr "సలహా / దోషము యొక్క  వివరణ"

#: ../src/bugzilla.c:468
#, c-format
msgid "HTTP Response returned bad status code %d"
msgstr "హెచ్  టిటిపి  స్పందన చెడు స్థితి కోడ్ %dను చూపించింది "

#: ../src/bugzilla.c:484
#, c-format
msgid ""
"Unable to parse XML-RPC Response\n"
"\n"
"%s"
msgstr ""
"XML-RPC యొక్క స్పందనను పార్స్  చేయలేకపొయింది\n"
"\n"
"%s"

#: ../src/bugzilla.c:516
#, c-format
msgid "Application does not track its bugs in the GNOME Bugzilla."
msgstr "GNOME Bugzilla లో  కార్యక్షేత్రము  లోపములను  పట్టు కోలేకపోయింది."

#: ../src/bugzilla.c:522
#, c-format
msgid "Product or component not specified."
msgstr "నిర్దేశిత  వస్తువు  లేదు."

#: ../src/bugzilla.c:617
#, c-format
msgid "Unable to create XML-RPC message."
msgstr "XML-RPC సందేశమును ఏర్పరుచటలో విఫలమైనది."

#: ../src/gdb-buddy.c:50
msgid "gdb has already exited"
msgstr "gdb ను ముందుగానే  కలిగి  ఉన్నది."

#: ../src/gdb-buddy.c:91
msgid "Error on read... aborting"
msgstr "చదువుటలో    దోషము   కలిగినది.కావున అర్థాంతరంగా   ఆపబడును."

#: ../src/gdb-buddy.c:259
#, c-format
msgid ""
"GDB could not be found on your system. Debugging information will not be "
"obtained."
msgstr "GDB మీ వ్యవస్థ   నందు  కనపడలేదు.లోపనిర్మూలన   సమాచారం   తీసుకోలేకపోయింది. "

#: ../src/gdb-buddy.c:268
#, c-format
msgid ""
"Could not find the gdb-cmd file.\n"
"Please try reinstalling Bug Buddy."
msgstr ""
"gdb-cmd అను దస్త్రము కనపడలేదు.\n"
"bug-buddy ను  దయ చేసి  మరల ప్రతిష్టించండి."

#: ../src/gdb-buddy.c:283
#, c-format
msgid ""
"There was an error running gdb:\n"
"\n"
"%s"
msgstr ""
"gdb ను  నడుపు సమయములో దోషము ఏర్పడినది: \n"
"\n"
"%s"

